<?php

/**
 * DAO for {@link ProfileAttachment}.
 * <p>
 * It is also a service, ideally, this class should be divided into DAO and Service.
 */
final class ProfileAttachmentDao {

    /** @var PDO */
    private $db = null;


    public function __destruct() {
        // close db connection
        $this->db = null;
    }

    /**
     * Find all {@link ProfileAttachment}s by search criteria.
     * @return array array of {@link ProfileAttachment}s
     */
    public function find($search = array()) {
        $result = array();
        foreach ($this->query($this->getFindSql($search)) as $row) {
            $profileAttachment = new ProfileAttachment();
            ProfileAttachmentMapper::map($profileAttachment, $row);
            $result[$profileAttachment->getId()] = $profileAttachment;
        }
        return $result;
    }

    /**
     * Find {@link ProfileAttachment} by identifier.
     * @return ProfileAttachment or <i>null</i> if not found
     */
    public function findByEmail($email) {
        $row = $this->query('SELECT * FROM dt_profile_attachment WHERE deleted = 0 and email_address = \'' . $email . '\'')->fetch();
        if (!$row) {
            return null;
        }
        $profileAttachment = new ProfileAttachment();
        ProfileAttachmentMapper::map($profileAttachment, $row);
        return $profileAttachment;
    }

    /**
     * Find {@link ProfileAttachment} by identifier.
     * @return ProfileAttachment or <i>null</i> if not found
     */
    public function findById($id) {
        $row = $this->query('SELECT * FROM dt_profile_attachment WHERE deleted = 0 and id = ' . $id)->fetch();
        if (!$row) {
            return null;
        }
        $profileAttachment = new ProfileAttachment();
        ProfileAttachmentMapper::map($profileAttachment, $row);
        return $profileAttachment;
    }

    /**
     * Save {@link ProfileAttachment}.
     * @param ProfileAttachment $profileAttachment {@link ProfileAttachment} to be saved
     * @return ProfileAttachment saved {@link ProfileAttachment} instance
     */
    public function save(ProfileAttachment $profileAttachment) {
        if (!$profileAttachment->getId()) {
            return $this->insert($profileAttachment);
        }
        return $this->update($profileAttachment);
    }

    /**
     * Delete {@link ProfileAttachment} by identifier.
     * @param int $id {@link ProfileAttachment} identifier
     * @return bool <i>true</i> on success, <i>false</i> otherwise
     */
    public function delete($id) {
        $sql = '
            UPDATE dt_profile_attachment SET
                last_modified_on = :last_modified_on,
                deleted = :deleted
            WHERE
                id = :id';
        $statement = $this->getDb()->prepare($sql);
        $this->executeStatement($statement, array(
            ':last_modified_on' => self::formatDateTime(new DateTime()),
            ':deleted' => true,
            ':id' => $id,
        ));
        return $statement->rowCount() == 1;
    }

    /**
     * @return PDO
     */
    private function getDb() {
        if ($this->db !== null) {
            return $this->db;
        }
        $config = Config::getConfig("db");
        try {
            $this->db = new PDO($config['dsn'], $config['username'], $config['password']);
        } catch (Exception $ex) {
            throw new Exception('DB connection error: ' . $ex->getMessage());
        }
        return $this->db;
    }
    
    public function setDb($db) {
        $this->db = $db;
    }

    private function getFindSql($search = array()) {
        $sql = 'SELECT * FROM dt_profile_attachment WHERE deleted = 0 ';
        $orderBy = ' id';
        if (isset($search['profile_id']) && $search['profile_id'] !== null) {
            $sql .= 'AND profile_id = ' . $this->getDb()->quote($search['profile_id']);
        }
        $sql .= ' ORDER BY ' . $orderBy;
        return $sql;
    }

    /**
     * @return ProfileAttachment
     * @throws Exception
     */
    private function insert(ProfileAttachment $profileAttachment) {
        $now = new DateTime();
        $profileAttachment->setId(null);
        $profileAttachment->setCreatedOn($now);
        $profileAttachment->setUpdatedOn($now);
        $sql =
            'INSERT INTO dt_profile_attachment ' .
                '(profile_id, type, filename, comments, uploaded_on, ' .
                ' deleted, created_on, updated_on, contents, filetype) ' .
            'VALUES '.
                '(:profile_id, :type, :filename, :comments, :uploaded_on, ' .
                ' :deleted, :created_on, :updated_on, :contents, :filetype)';
        return $this->execute($sql, $profileAttachment);
    }

    /**
     * @return ProfileAttachment
     * @throws Exception
     */
    private function update(ProfileAttachment $profileAttachment) {
        $profileAttachment->setUpdatedOn(new DateTime());
        $sql = '
            UPDATE dt_profile_attachment SET
                type = :type,
                filename = :filename,
                filetype = :filetype,
                contents = :contents,
                comments = :comments,
                uploaded_on = :uploaded_on,
                deleted = :deleted,
                updated_on = :updated_on
            WHERE
                id = :id';
        return $this->execute($sql, $profileAttachment);
    }

    /**
     * @return ProfileAttachment
     * @throws Exception
     */
    private function execute($sql, ProfileAttachment $profileAttachment) {
        $sqlStr = $this->parms($sql, $this->getParams($profileAttachment));
        $statement = $this->getDb()->prepare($sqlStr);
        $this->executeStatement($statement, $this->getParams($profileAttachment));
        if (!$profileAttachment->getId()) {
            return $this->findById($this->getDb()->lastInsertId());
        }
        if (!$statement->rowCount()) {
            throw new NotFoundException('Profile Attachment with ID "' . $profileAttachment->getId() . '" does not exist.');
        }
        return $profileAttachment;
    }

    private function getParams(ProfileAttachment $profileAttachment) {
        $params = array(
            ':id' => $profileAttachment->getId(),
            ':profile_id' => $profileAttachment->getProfileId(),
            ':type' => $profileAttachment->getType(),
            ':filename' => $profileAttachment->getFilename(),
            ':filetype' => $profileAttachment->getFiletype(),
            ':contents' => $profileAttachment->getContents(),
            ':comments' => $profileAttachment->getComments(),
            ':uploaded_on' => $profileAttachment->getUploadedOn(),
            ':deleted' => $profileAttachment->getDeleted() ? 1 : 0,
            ':created_on' => date('Y-m-d H:i:s'),
            ':updated_on' => date('Y-m-d H:i:s')
        );
        if ($profileAttachment->getId()) {
            // unset created date, this one is never updated
            unset($params[':created_on']);
        }
        return $params;
    }

    private function executeStatement(PDOStatement $statement, array $params) {
        if (!$statement->execute($params)) {
            self::throwDbError($this->getDb()->errorInfo());
        }
    }
    
    private function parms($string,$data) {
        $indexed=$data==array_values($data);
        foreach($data as $k=>$v) {
            if(is_string($v)) $v="'$v'";
            if($indexed) $string=preg_replace('/\?/',$v,$string,1);
            else $string=str_replace("$k",$v,$string);
        }
        return $string;
    }

    /**
     * @return PDOStatement
     */
    private function query($sql) {
        $statement = $this->getDb()->query($sql, PDO::FETCH_ASSOC);
        if ($statement === false) {
            self::throwDbError($this->getDb()->errorInfo());
        }
        return $statement;
    }

    private static function throwDbError(array $errorInfo) {
        // TODO log error, send email, etc.
        throw new Exception('DB error [' . $errorInfo[0] . ', ' . $errorInfo[1] . ']: ' . $errorInfo[2]);
    }

    private static function formatDateTime(DateTime $date) {
        return $date->format(DateTime::ISO8601);
    }

}
