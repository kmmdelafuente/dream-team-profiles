<?php

/**
 * DAO for {@link Profile}.
 * <p>
 * It is also a service, ideally, this class should be divided into DAO and Service.
 */
final class ProfileDao {

    /** @var PDO */
    private $db = null;

    public function __destruct() {
        // close db connection
        $this->db = null;
    }

    /**
     * Find all {@link Profile}s by search criteria.
     * @return array array of {@link Profile}s
     */
    public function find(ProfileSearchCriteria $search = null) {
        $result = array();
        foreach ($this->query($this->getFindSql($search)) as $row) {
            $profile = new Profile();
            ProfileMapper::map($profile, $row);
            $result[$profile->getId()] = $profile;
        }
        return $result;
    }

    /**
     * Find {@link Profile} by identifier.
     * @return Profile or <i>null</i> if not found
     */
    public function findByEmail($email) {
        $row = $this->query('SELECT * FROM dt_profile WHERE deleted = 0 and email_address = \'' . $email . '\'')->fetch();
        if (!$row) {
            return null;
        }
        return $this->getProfile($row);
    }

    /**
     * Find {@link Profile} by identifier.
     * @return Profile or <i>null</i> if not found
     */
    public function findById($id) {
        $row = $this->query('SELECT * FROM dt_profile WHERE deleted = 0 and id = ' . $id)->fetch();
        if (!$row) {
            return null;
        }
        return $this->getProfile($row);
    }

    private function getProfile($profileArr) {
        $profile = new Profile();
        ProfileMapper::map($profile, $profileArr);

        $attachmentDao = new ProfileAttachmentDao();
        $result = $attachmentDao->find(['profile_id' => $profile->getId()]);
        $profile->setAttachments($result);

        $licenseDao = new ProfileLicenseDao();
        $result = $licenseDao->find(['profile_id' => $profile->getId()]);
        $profile->setLicenses($result);
        return $profile;
    }

    /**
     * Save {@link Profile}.
     * @param Profile $profile {@link Profile} to be saved
     * @return Profile saved {@link Profile} instance
     */
    public function save(Profile $profile) {
        $thisProfile = null;
        //$this->getDb()->setAttribute(PDO::ATTR_AUTOCOMMIT,0);
        try {
            if (!$profile->getId()) {
                $thisProfile = $this->insert($profile);
            } else {
                $thisProfile = $this->update($profile);
            }
        } catch (Exception $exc) {
            //$this->getDb()->rollBack();
            return array("succeed" => false, "msg" => $exc->getTraceAsString());
        }

        //$this->getDb()->commit();
        return array("succeed" => true, "profile" => $thisProfile->toArray());
    }

    /**
     * Delete {@link Profile} by identifier.
     * @param int $id {@link Profile} identifier
     * @return bool <i>true</i> on success, <i>false</i> otherwise
     */
    public function delete($id) {
        $sql = '
            UPDATE dt_profile SET
                last_modified_on = :last_modified_on,
                deleted = :deleted
            WHERE
                id = :id';
        $statement = $this->getDb()->prepare($sql);
        $this->executeStatement($statement, array(
            ':last_modified_on' => self::formatDateTime(new DateTime()),
            ':deleted' => true,
            ':id' => $id,
        ));
        return $statement->rowCount() == 1;
    }

    /**
     * @return PDO
     */
    private function getDb() {
        if ($this->db !== null) {
            return $this->db;
        }
        $config = Config::getConfig("db");
        try {
            $this->db = new PDO($config['dsn'], $config['username'], $config['password']);
        } catch (Exception $ex) {
            throw new Exception('DB connection error: ' . $ex->getMessage());
        }
        return $this->db;
    }

    private function getFindSql(ProfileSearchCriteria $search = null) {
        $sql = 'SELECT * FROM dt_profile WHERE deleted = 0 ';
        $orderBy = ' priority, due_on';
        if ($search !== null) {
            if ($search->getStatus() !== null) {
                $sql .= 'AND status = ' . $this->getDb()->quote($search->getStatus());
                switch ($search->getStatus()) {
                    case Profile::STATUS_PENDING:
                        $orderBy = 'due_on, priority';
                        break;
                    case Profile::STATUS_DONE:
                    case Profile::STATUS_VOIDED:
                        $orderBy = 'due_on DESC, priority';
                        break;
                    default:
                        throw new Exception('No order for status: ' . $search->getStatus());
                }
            }
        }
        $sql .= ' ORDER BY ' . $orderBy;
        return $sql;
    }

    public function changePassword(Profile $profile) {
        try {
            $profile->setUpdatedOn(new DateTime());
            $sql = '
                UPDATE dt_profile SET password = :password, updated_on = :updated_on
                WHERE id = :id';
            $result = $this->execute($sql, $profile);
            return array("succeed" => true, "profile" => $result->toArray());
        } catch (Exception $exc) {
            return array("succeed" => false, "msg" => $exc->getTraceAsString());
        }
    }

    /**
     * @return Profile
     * @throws Exception
     */
    private function insert(Profile $profile) {
        $now = new DateTime();
        $profile->setId(null);
        $profile->setCreatedOn($now);
        $profile->setUpdatedOn($now);
//        $sql = '
//            INSERT INTO dt_profile (password, first_name, middle_name, last_name, home_phone, ' .
//                'cell_phone, time_to_call, profession, address, city, state, country, email_address, ' .
//                'zip_code, languages, other_languages, specialties, start_time, total_hours, ' .
//                'lead, comments, deleted, created_on, updated_on)
//                VALUES (:password, :first_name, :middle_name, :last_name, :home_phone, ' .
//                ':cell_phone, :time_to_call, :profession, :address, :city, :state, :country, :email_address, ' .
//                ':zip_code, :languages, :other_languages, :specialties, :start_time, :total_hours, ' .
//                ':lead, :comments, :deleted, :created_on, :updated_on)';
        $sql = '
            INSERT INTO dt_profile (password, email_address, first_name, last_name, created_on, updated_on) ' .
                'VALUES (:password, :email_address, :first_name, :last_name, :created_on, :updated_on)';
        return $this->execute($sql, $profile);
    }

    /**
     * @return Profile
     * @throws Exception
     */
    private function update(Profile $profile) {
        $profile->setUpdatedOn(new DateTime());
        $sql = '
            UPDATE dt_profile SET ' .
                'first_name = :first_name, ' .
                'middle_name = :middle_name, ' .
                'last_name = :last_name, ' .
                'home_phone = :home_phone, ' .
                'cell_phone = :cell_phone, ' .
                'time_to_call = :time_to_call, ' .
                'profession = :profession, ' .
                'address = :address, ' .
                'city = :city, ' .
                'state = :state, ' .
                'country = :country, ' .
                'zip_code = :zip_code, ' .
                'languages = :languages, ' .
                'other_languages = :other_languages, ' .
                'specialties = :specialties, ' .
                'start_time = :start_time, ' .
                'total_hours = :total_hours, ' .
                'lead = :lead, ' .
                'comments = :comments, ' .
                'deleted = :deleted, ' .
                'updated_on = :updated_on ' .
            'WHERE ' .
                'id = :id';
        return $this->execute($sql, $profile);
    }

    /**
     * @return Profile
     * @throws Exception
     */
    private function execute($sql, Profile $profile) {
        $insertedProfileId = null;
        $sqlStr = $this->parms($sql, $this->getParams($profile));
        //$statement = $this->getDb()->prepare($sql);
        $statement = $this->getDb()->prepare($sqlStr);
        $this->executeStatement($statement, $this->getParams($profile));

        if (!$profile->getId()) {
            $insertedProfileId = $this->getDb()->lastInsertId();
        }

        if (!$statement->rowCount()) {
            throw new NotFoundException('Profile with ID "' . $profile->getId() . '" does not exist.');
        }
        $this->saveAttachments($profile, $insertedProfileId);
        $this->saveLicenses($profile, $insertedProfileId);

        $updatedProfile = $this->findById($insertedProfileId ? $insertedProfileId : $profile->getId());

        return $updatedProfile;
    }

    private function saveAttachments(Profile $profile, $insertedProfileId) {
        $attachments = $profile->getAttachments();
        foreach ($attachments as $attachment) {
            if (!$attachment->getId() && $attachment->getDeleted()) {
                continue;
            }

            if ($insertedProfileId) {
                $attachment->setProfileId($insertedProfileId);
            } else if (!$attachment->getProfileId()) {
                $attachment->setProfileId($profile->getId());
            }

            $dao = new ProfileAttachmentDao();
            $dao->setDb($this->getDb());
            $profileAttachment = $dao->save($attachment);
        }
    }

    private function saveLicenses(Profile $profile, $insertedProfileId) {
        $licenses = $profile->getLicenses();
        foreach ($licenses as $license) {
            if (!$license->getId() && $license->getDeleted()) {
                continue;
            }

            if ($insertedProfileId) {
                $license->setProfileId($insertedProfileId);
            } else if (!$license->getProfileId()) {
                $license->setProfileId($profile->getId());
            }

            $dao = new ProfileLicenseDao();
            $dao->setDb($this->getDb());
            $profileLicense = $dao->save($license);
        }
    }

    private function getParams(Profile $profile) {
        $params = array(
            ':id' => $profile->getId(),
            ':password' => $profile->getPassword(),
            ':first_name' => $profile->getFirstName(),
            ':middle_name' => $profile->getMiddleName(),
            ':last_name' => $profile->getLastName(),
            ':home_phone' => $profile->getHomePhone(),
            ':cell_phone' => $profile->getCellPhone(),
            ':time_to_call' => $profile->getTimeToCall(),
            ':profession' => $profile->getProfession(),
            ':address' => $profile->getAddress(),
            ':city' => $profile->getCity(),
            ':state' => $profile->getState(),
            ':country' => $profile->getCountry(),
            ':email_address' => $profile->getEmailAddress(),
            ':zip_code' => $profile->getZipCode(),
            ':languages' => $profile->getLanguages(),
            ':other_languages' => $profile->getOtherLanguages(),
            ':specialties' => $profile->getSpecialties(),
            ':start_time' => $profile->getStartTime(),
            ':total_hours' => $profile->getTotalHours(),
            ':lead' => $profile->getLead(),
            ':comments' => $profile->getComments(),
            ':deleted' => $profile->getDeleted(),
            ':created_on' => date('Y-m-d H:i:s'),
            ':updated_on' => date('Y-m-d H:i:s')
        );
        if ($profile->getId()) {
            // unset created date, this one is never updated
            unset($params[':created_on']);
        }
        return $params;
    }

    private function executeStatement(PDOStatement $statement, array $params) {
        if (!$statement->execute($params)) {
            //self::throwDbError($statement->debugDumpParams());
            self::throwDbError($this->getDb()->errorInfo());
        }
    }

    private function parms($string, $data) {
        $indexed = $data == array_values($data);
        foreach ($data as $k => $v) {
            if (is_string($v))
                $v = "'$v'";
            if ($indexed)
                $string = preg_replace('/\?/', $v, $string, 1);
            else
                $string = str_replace("$k", $v, $string);
        }
        return $string;
    }

    /**
     * @return PDOStatement
     */
    private function query($sql) {
        $statement = $this->getDb()->query($sql, PDO::FETCH_ASSOC);
        if ($statement === false) {
            self::throwDbError($this->getDb()->errorInfo());
        }
        return $statement;
    }

    private static function throwDbError(array $errorInfo) {
        // TODO log error, send email, etc.
        throw new Exception('DB error [' . $errorInfo[0] . ', ' . $errorInfo[1] . ']: ' . $errorInfo[2]);
    }

    private static function formatDateTime(DateTime $date) {
        return $date->format(DateTime::ISO8601);
    }

}
