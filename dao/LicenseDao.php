<?php

/**
 * DAO for {@link License}.
 * <p>
 * It is also a service, ideally, this class should be divided into DAO and Service.
 */
final class LicenseDao {

    /** @var PDO */
    private $db = null;


    public function __destruct() {
        // close db connection
        $this->db = null;
    }

    /**
     * Find all {@link License}s by search criteria.
     * @return array array of {@link License}s
     */
    public function find($search = array()) {
        $result = array();
        foreach ($this->query($this->getFindSql($search)) as $row) {
            $license = new License();
            LicenseMapper::map($license, $row);
            $result[$license->getId()] = $license;
        }
        return $result;
    }

    /**
     * Find {@link License} by identifier.
     * @return License or <i>null</i> if not found
     */
    public function findByEmail($email) {
        $row = $this->query('SELECT * FROM dt_license WHERE deleted = 0 and email_address = \'' . $email . '\'')->fetch();
        if (!$row) {
            return null;
        }
        $license = new License();
        LicenseMapper::map($license, $row);
        return $license;
    }

    /**
     * Find {@link License} by identifier.
     * @return License or <i>null</i> if not found
     */
    public function findById($id) {
        $row = $this->query('SELECT * FROM dt_license WHERE deleted = 0 and id = ' . $id)->fetch();
        if (!$row) {
            return null;
        }
        $license = new License();
        LicenseMapper::map($license, $row);
        return $license;
    }

    /**
     * Save {@link License}.
     * @param License $license {@link License} to be saved
     * @return License saved {@link License} instance
     */
    public function save(License $license) {
        if (!$license->getId()) {
            return $this->insert($license);
        }
        return $this->update($license);
    }

    /**
     * Delete {@link License} by identifier.
     * @param int $id {@link License} identifier
     * @return bool <i>true</i> on success, <i>false</i> otherwise
     */
    public function delete($id) {
        $sql = '
            UPDATE dt_license SET
                last_modified_on = :last_modified_on,
                deleted = :deleted
            WHERE
                id = :id';
        $statement = $this->getDb()->prepare($sql);
        $this->executeStatement($statement, array(
            ':last_modified_on' => self::formatDateTime(new DateTime()),
            ':deleted' => true,
            ':id' => $id,
        ));
        return $statement->rowCount() == 1;
    }

    /**
     * @return PDO
     */
    private function getDb() {
        if ($this->db !== null) {
            return $this->db;
        }
        $config = Config::getConfig("db");
        try {
            $this->db = new PDO($config['dsn'], $config['username'], $config['password']);
        } catch (Exception $ex) {
            throw new Exception('DB connection error: ' . $ex->getMessage());
        }
        return $this->db;
    }
    
    public function setDb($db) {
        $this->db = $db;
    }

    private function getFindSql($search = array()) {
        $sql = 'SELECT * FROM dt_license WHERE deleted = 0 ';
        $orderBy = ' type';
        if (isset($search['territory']) && $search['territory'] !== null) {
            $sql .= 'AND territory = ' . $this->getDb()->quote($search['territory']);
        }
        $sql .= ' ORDER BY ' . $orderBy;
        return $sql;
    }

    /**
     * @return License
     * @throws Exception
     */
    private function insert(License $license) {
        $now = new DateTime();
        $license->setId(null);
        $license->setCreatedOn($now);
        $license->setUpdatedOn($now);
        $sql =
            'INSERT INTO dt_license ' .
                '(type, territory, deleted, created_on, updated_on) ' .
            'VALUES '.
                '(:type, :territory, :deleted, :created_on, :updated_on)';
        return $this->execute($sql, $license);
    }

    /**
     * @return License
     * @throws Exception
     */
    private function update(License $license) {
        $license->setUpdatedOn(new DateTime());
        $sql = '
            UPDATE dt_license SET
                type = :type,
                territory = :territory,
                uploaded_on = :uploaded_on,
                deleted = :deleted,
                updated_on = :updated_on
            WHERE
                id = :id';
        return $this->execute($sql, $license);
    }

    /**
     * @return License
     * @throws Exception
     */
    private function execute($sql, License $license) {
        $sqlStr = $this->parms($sql, $this->getParams($license));
        $statement = $this->getDb()->prepare($sqlStr);
        $this->executeStatement($statement, $this->getParams($license));
        if (!$license->getId()) {
            return $this->findById($this->getDb()->lastInsertId());
        }
        if (!$statement->rowCount()) {
            throw new NotFoundException('Profile Attachment with ID "' . $license->getId() . '" does not exist.');
        }
        return $license;
    }

    private function getParams(License $license) {
        $params = array(
            ':id' => $license->getId(),
            ':territory' => $license->getTerritory(),
            ':type' => $license->getType(),
            ':deleted' => $license->getDeleted() ? 1 : 0,
            ':created_on' => date('Y-m-d H:i:s'),
            ':updated_on' => date('Y-m-d H:i:s')
        );
        if ($license->getId()) {
            // unset created date, this one is never updated
            unset($params[':created_on']);
        }
        return $params;
    }

    private function executeStatement(PDOStatement $statement, array $params) {
        if (!$statement->execute($params)) {
            self::throwDbError($this->getDb()->errorInfo());
        }
    }
    
    private function parms($string,$data) {
        $indexed=$data==array_values($data);
        foreach($data as $k=>$v) {
            if(is_string($v)) $v="'$v'";
            if($indexed) $string=preg_replace('/\?/',$v,$string,1);
            else $string=str_replace("$k",$v,$string);
        }
        return $string;
    }

    /**
     * @return PDOStatement
     */
    private function query($sql) {
        $statement = $this->getDb()->query($sql, PDO::FETCH_ASSOC);
        if ($statement === false) {
            self::throwDbError($this->getDb()->errorInfo());
        }
        return $statement;
    }

    private static function throwDbError(array $errorInfo) {
        // TODO log error, send email, etc.
        throw new Exception('DB error [' . $errorInfo[0] . ', ' . $errorInfo[1] . ']: ' . $errorInfo[2]);
    }

    private static function formatDateTime(DateTime $date) {
        return $date->format(DateTime::ISO8601);
    }

}
