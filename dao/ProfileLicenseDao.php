<?php

/**
 * DAO for {@link ProfileLicense}.
 * <p>
 * It is also a service, ideally, this class should be divided into DAO and Service.
 */
final class ProfileLicenseDao {

    /** @var PDO */
    private $db = null;


    public function __destruct() {
        // close db connection
        $this->db = null;
    }

    /**
     * Find all {@link ProfileLicense}s by search criteria.
     * @return array array of {@link ProfileLicense}s
     */
    public function find($search = array()) {
        $result = array();
        foreach ($this->query($this->getFindSql($search)) as $row) {
            $profileLicense = new ProfileLicense();
            ProfileLicenseMapper::map($profileLicense, $row);
            $result[$profileLicense->getId()] = $profileLicense;
        }
        return $result;
    }

    /**
     * Find {@link ProfileLicense} by identifier.
     * @return ProfileLicense or <i>null</i> if not found
     */
    public function findByEmail($email) {
        $row = $this->query('SELECT * FROM dt_profile_license WHERE deleted = 0 and email_address = \'' . $email . '\'')->fetch();
        if (!$row) {
            return null;
        }
        $profileLicense = new ProfileLicense();
        ProfileLicenseMapper::map($profileLicense, $row);
        return $profileLicense;
    }

    /**
     * Find {@link ProfileLicense} by identifier.
     * @return ProfileLicense or <i>null</i> if not found
     */
    public function findById($id) {
        $row = $this->query('SELECT * FROM dt_profile_license WHERE deleted = 0 and id = ' . $id)->fetch();
        if (!$row) {
            return null;
        }
        $profileLicense = new ProfileLicense();
        ProfileLicenseMapper::map($profileLicense, $row);
        return $profileLicense;
    }

    /**
     * Save {@link ProfileLicense}.
     * @param ProfileLicense $profileLicense {@link ProfileLicense} to be saved
     * @return ProfileLicense saved {@link ProfileLicense} instance
     */
    public function save(ProfileLicense $profileLicense) {
        if (!$profileLicense->getId()) {
            return $this->insert($profileLicense);
        }
        return $this->update($profileLicense);
    }

    /**
     * Delete {@link ProfileLicense} by identifier.
     * @param int $id {@link ProfileLicense} identifier
     * @return bool <i>true</i> on success, <i>false</i> otherwise
     */
    public function delete($id) {
        $sql = '
            UPDATE dt_profile_license SET
                last_modified_on = :last_modified_on,
                deleted = :deleted
            WHERE
                id = :id';
        $statement = $this->getDb()->prepare($sql);
        $this->executeStatement($statement, array(
            ':last_modified_on' => self::formatDateTime(new DateTime()),
            ':deleted' => true,
            ':id' => $id,
        ));
        return $statement->rowCount() == 1;
    }

    /**
     * @return PDO
     */
    private function getDb() {
        if ($this->db !== null) {
            return $this->db;
        }
        $config = Config::getConfig("db");
        try {
            $this->db = new PDO($config['dsn'], $config['username'], $config['password']);
        } catch (Exception $ex) {
            throw new Exception('DB connection error: ' . $ex->getMessage());
        }
        return $this->db;
    }
    
    public function setDb($db) {
        $this->db = $db;
    }

    private function getFindSql($search = array()) {
        $sql = 'SELECT * FROM dt_profile_license WHERE deleted = 0 ';
        $orderBy = ' id';
        if (isset($search['profile_id']) && $search['profile_id'] !== null) {
            $sql .= 'AND profile_id = ' . $this->getDb()->quote($search['profile_id']);
        }
        $sql .= ' ORDER BY ' . $orderBy;
        return $sql;
    }

    /**
     * @return ProfileLicense
     * @throws Exception
     */
    private function insert(ProfileLicense $profileLicense) {
        $now = new DateTime();
        $profileLicense->setId(null);
        $profileLicense->setCreatedOn($now);
        $profileLicense->setUpdatedOn($now);
        $sql =
            'INSERT INTO dt_profile_license ' .
                '(profile_id, license_id, comments, reference_number, ' .
                ' expiry_date, deleted, created_on, updated_on) ' .
            'VALUES '.
                '(:profile_id, :license_id, :comments, :reference_number, ' .
                ' :expiry_date, :deleted, :created_on, :updated_on)';
        return $this->execute($sql, $profileLicense);
    }

    /**
     * @return ProfileLicense
     * @throws Exception
     */
    private function update(ProfileLicense $profileLicense) {
        $profileLicense->setUpdatedOn(new DateTime());
        $sql = '
            UPDATE dt_profile_license SET
                license_id = :license_id,
                comments = :comments,
                reference_number = :reference_number,
                expiry_date = :expiry_date,
                deleted = :deleted,
                updated_on = :updated_on
            WHERE
                id = :id';
        return $this->execute($sql, $profileLicense);
    }

    /**
     * @return ProfileLicense
     * @throws Exception
     */
    private function execute($sql, ProfileLicense $profileLicense) {
        $sqlStr = $this->parms($sql, $this->getParams($profileLicense));
        $statement = $this->getDb()->prepare($sqlStr);
        $this->executeStatement($statement, $this->getParams($profileLicense));
        if (!$profileLicense->getId()) {
            return $this->findById($this->getDb()->lastInsertId());
        }
        if (!$statement->rowCount()) {
            throw new NotFoundException('Profile Attachment with ID "' . $profileLicense->getId() . '" does not exist.');
        }
        return $profileLicense;
    }

    private function getParams(ProfileLicense $profileLicense) {
        $params = array(
            ':id' => $profileLicense->getId(),
            ':profile_id' => $profileLicense->getProfileId(),
            ':license_id' => $profileLicense->getLicenseId(),
            ':comments' => $profileLicense->getComments(),
            ':reference_number' => $profileLicense->getReferenceNumber(),
            ':expiry_date' => $profileLicense->getExpiryDate(),
            ':deleted' => $profileLicense->getDeleted() ? 1 : 0,
            ':created_on' => date('Y-m-d H:i:s'),
            ':updated_on' => date('Y-m-d H:i:s')
        );
        if ($profileLicense->getId()) {
            // unset created date, this one is never updated
            unset($params[':created_on']);
        }
        return $params;
    }

    private function executeStatement(PDOStatement $statement, array $params) {
        if (!$statement->execute($params)) {
            self::throwDbError($this->getDb()->errorInfo());
        }
    }
    
    private function parms($string,$data) {
        $indexed=$data==array_values($data);
        foreach($data as $k=>$v) {
            if(is_string($v)) $v="'$v'";
            if($indexed) $string=preg_replace('/\?/',$v,$string,1);
            else $string=str_replace("$k",$v,$string);
        }
        return $string;
    }

    /**
     * @return PDOStatement
     */
    private function query($sql) {
        $statement = $this->getDb()->query($sql, PDO::FETCH_ASSOC);
        if ($statement === false) {
            self::throwDbError($this->getDb()->errorInfo());
        }
        return $statement;
    }

    private static function throwDbError(array $errorInfo) {
        // TODO log error, send email, etc.
        throw new Exception('DB error [' . $errorInfo[0] . ', ' . $errorInfo[1] . ']: ' . $errorInfo[2]);
    }

    private static function formatDateTime(DateTime $date) {
        return $date->format(DateTime::ISO8601);
    }

}
