<?php

/**
 * 
 *
 * @version 1.107
 * @package entity
 */
class License {

    private static $CLASS_NAME = 'License';

    const SQL_IDENTIFIER_QUOTE = '`';
    const SQL_TABLE_NAME = 'dt_license';
    const SQL_INSERT = 'INSERT INTO `dt_license` (`id`,`territory`,`type`,`deleted`,`created_on`,`updated_on`) VALUES (?,?,?,?,?,?)';
    const SQL_INSERT_AUTOINCREMENT = 'INSERT INTO `dt_license` (`territory`,`type`,`deleted`,`created_on`,`updated_on`) VALUES (?,?,?,?,?)';
    const SQL_UPDATE = 'UPDATE `dt_license` SET `id`=?,`territory`=?,`type`=?,`deleted`=?,`created_on`=?,`updated_on`=? WHERE `id`=?';
    const SQL_SELECT_PK = 'SELECT * FROM `dt_license` WHERE `id`=?';
    const SQL_DELETE_PK = 'DELETE FROM `dt_license` WHERE `id`=?';
    const FIELD_ID = -1799679753;
    const FIELD_TERRITORY = 500438206;
    const FIELD_TYPE = 1379929110;
    const FIELD_DELETED = 444806589;
    const FIELD_CREATED_ON = 863703666;
    const FIELD_UPDATED_ON = -801440833;

    private static $PRIMARY_KEYS = array(self::FIELD_ID);
    private static $AUTOINCREMENT_FIELDS = array(self::FIELD_ID);
    private static $FIELD_NAMES = array(
        self::FIELD_ID => 'id',
        self::FIELD_TERRITORY => 'territory',
        self::FIELD_TYPE => 'type',
        self::FIELD_DELETED => 'deleted',
        self::FIELD_CREATED_ON => 'created_on',
        self::FIELD_UPDATED_ON => 'updated_on');
    private static $PROPERTY_NAMES = array(
        self::FIELD_ID => 'id',
        self::FIELD_TERRITORY => 'territory',
        self::FIELD_TYPE => 'type',
        self::FIELD_DELETED => 'deleted',
        self::FIELD_CREATED_ON => 'createdOn',
        self::FIELD_UPDATED_ON => 'updatedOn');
    private static $DEFAULT_VALUES = array(
        self::FIELD_ID => null,
        self::FIELD_TERRITORY => null,
        self::FIELD_TYPE => null,
        self::FIELD_DELETED => '0',
        self::FIELD_CREATED_ON => '0000-00-00 00:00:00',
        self::FIELD_UPDATED_ON => 'CURRENT_TIMESTAMP');
    private $id;
    private $territory;
    private $type;
    private $deleted;
    private $createdOn;
    private $updatedOn;

    /**
     * set value for id 
     *
     * type:INT UNSIGNED,size:10,default:null,primary,unique,autoincrement
     *
     * @param mixed $id
     */
    public function setId($id) {
        $this->id = $id;
    }

    /**
     * get value for id 
     *
     * type:INT UNSIGNED,size:10,default:null,primary,unique,autoincrement
     *
     * @return mixed
     */
    public function getId() {
        return $this->id;
    }

    /**
     * set value for territory 
     *
     * type:VARCHAR,size:50,default:null,nullable
     *
     * @param mixed $territory
     */
    public function setTerritory($territory) {
        $this->territory = $territory;
    }

    /**
     * get value for territory 
     *
     * type:VARCHAR,size:50,default:null,nullable
     *
     * @return mixed
     */
    public function getTerritory() {
        return $this->territory;
    }

    /**
     * set value for type 
     *
     * type:VARCHAR,size:50,default:null,nullable
     *
     * @param mixed $type
     */
    public function setType($type) {
        $this->type = $type;
    }

    /**
     * get value for type 
     *
     * type:VARCHAR,size:50,default:null,nullable
     *
     * @return mixed
     */
    public function getType() {
        return $this->type;
    }

    /**
     * set value for deleted 
     *
     * type:BIT,size:0,default:0
     *
     * @param mixed $deleted
     */
    public function setDeleted($deleted) {
        $this->deleted = $deleted;
    }

    /**
     * get value for deleted 
     *
     * type:BIT,size:0,default:0
     *
     * @return mixed
     */
    public function getDeleted() {
        return $this->deleted;
    }

    /**
     * set value for created_on 
     *
     * type:TIMESTAMP,size:19,default:0000-00-00 00:00:00
     *
     * @param mixed $createdOn
     */
    public function setCreatedOn($createdOn) {
        $this->createdOn = $createdOn;
    }

    /**
     * get value for created_on 
     *
     * type:TIMESTAMP,size:19,default:0000-00-00 00:00:00
     *
     * @return mixed
     */
    public function getCreatedOn() {
        return $this->createdOn;
    }

    /**
     * set value for updated_on 
     *
     * type:TIMESTAMP,size:19,default:CURRENT_TIMESTAMP
     *
     * @param mixed $updatedOn
     */
    public function setUpdatedOn($updatedOn) {
        $this->updatedOn = $updatedOn;
    }

    /**
     * get value for updated_on 
     *
     * type:TIMESTAMP,size:19,default:CURRENT_TIMESTAMP
     *
     * @return mixed
     */
    public function getUpdatedOn() {
        return $this->updatedOn;
    }

    /**
     * Get table name
     *
     * @return string
     */
    public static function getTableName() {
        return self::SQL_TABLE_NAME;
    }

    /**
     * Get array with field id as index and field name as value
     *
     * @return array
     */
    public static function getFieldNames() {
        return self::$FIELD_NAMES;
    }

    /**
     * Get array with field id as index and property name as value
     *
     * @return array
     */
    public static function getPropertyNames() {
        return self::$PROPERTY_NAMES;
    }

    /**
     * get the field name for the passed field id.
     *
     * @param int $fieldId
     * @param bool $fullyQualifiedName true if field name should be qualified by table name
     * @return string field name for the passed field id, null if the field doesn't exist
     */
    public static function getFieldNameByFieldId($fieldId, $fullyQualifiedName = true) {
        if (!array_key_exists($fieldId, self::$FIELD_NAMES)) {
            return null;
        }
        $fieldName = self::SQL_IDENTIFIER_QUOTE . self::$FIELD_NAMES[$fieldId] . self::SQL_IDENTIFIER_QUOTE;
        if ($fullyQualifiedName) {
            return self::SQL_IDENTIFIER_QUOTE . self::SQL_TABLE_NAME . self::SQL_IDENTIFIER_QUOTE . '.' . $fieldName;
        }
        return $fieldName;
    }

    /**
     * Get array with field ids of identifiers
     *
     * @return array
     */
    public static function getIdentifierFields() {
        return self::$PRIMARY_KEYS;
    }

    /**
     * Get array with field ids of autoincrement fields
     *
     * @return array
     */
    public static function getAutoincrementFields() {
        return self::$AUTOINCREMENT_FIELDS;
    }

    /**
     * Get array with field id as index and property type as value
     *
     * @return array
     */
    public static function getPropertyTypes() {
        return self::$PROPERTY_TYPES;
    }

    /**
     * Get array with field id as index and field type as value
     *
     * @return array
     */
    public static function getFieldTypes() {
        return self::$FIELD_TYPES;
    }

    /**
     * Assign default values according to table
     * 
     */
    public function assignDefaultValues() {
        $this->assignByArray(self::$DEFAULT_VALUES);
    }

    /**
     * return hash with the field name as index and the field value as value.
     *
     * @return array
     */
    public function toHash() {
        $array = $this->toArray();
        $hash = array();
        foreach ($array as $fieldId => $value) {
            $hash[self::$FIELD_NAMES[$fieldId]] = $value;
        }
        return $hash;
    }

    /**
     * return array with the field id as index and the field value as value.
     *
     * @return array
     */
    public function toArray() {
        return array(
            self::$PROPERTY_NAMES[self::FIELD_ID] => $this->getId(),
            self::$PROPERTY_NAMES[self::FIELD_TERRITORY] => $this->getTerritory(),
            self::$PROPERTY_NAMES[self::FIELD_TYPE] => $this->getType(),
            self::$PROPERTY_NAMES[self::FIELD_DELETED] => $this->getDeleted(),
            self::$PROPERTY_NAMES[self::FIELD_CREATED_ON] => $this->getCreatedOn(),
            self::$PROPERTY_NAMES[self::FIELD_UPDATED_ON] => $this->getUpdatedOn());
    }

    /**
     * return array with the field id as index and the field value as value for the identifier fields.
     *
     * @return array
     */
    public function getPrimaryKeyValues() {
        return array(
            self::FIELD_ID => $this->getId());
    }

    /**
     * Assign values from array with the field id as index and the value as value
     *
     * @param array $array
     */
    public function assignByArray($array) {
        $result = array();
        foreach ($array as $fieldId => $value) {
            $result[self::$FIELD_NAMES[$fieldId]] = $value;
        }
        $this->assignByHash($result);
    }

    /**
     * Assign values from hash where the indexes match the tables field names
     *
     * @param array $result
     */
    public function assignByHash($result) {
        $this->setId($result['id']);
        $this->setTerritory($result['territory']);
        $this->setType($result['type']);
        $this->setDeleted($result['deleted']);
        $this->setCreatedOn($result['created_on']);
        $this->setUpdatedOn($result['updated_on']);
    }

    /**
     * Bind all values to statement
     *
     * @param PDOStatement $stmt
     */
    protected function bindValues(PDOStatement &$stmt) {
        $stmt->bindValue(1, $this->getId());
        $stmt->bindValue(2, $this->getTerritory());
        $stmt->bindValue(3, $this->getType());
        $stmt->bindValue(4, $this->getDeleted());
        $stmt->bindValue(5, $this->getCreatedOn());
        $stmt->bindValue(6, $this->getUpdatedOn());
    }

}
