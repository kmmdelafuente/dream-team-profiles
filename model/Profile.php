<?php

/**
 * 
 *
 * @version 1.107
 * @package entity
 */
class Profile {
    
    const SQL_IDENTIFIER_QUOTE = '`';
    const SQL_TABLE_NAME = 'dt_profile';
    const FIELD_ID = -1496173937;
    const FIELD_EMAIL_ADDRESS = 1031134301;
    const FIELD_PASSWORD = 1220475151;
    const FIELD_FIRST_NAME = -1102643154;
    const FIELD_MIDDLE_NAME = 1294453761;
    const FIELD_LAST_NAME = 2121293472;
    const FIELD_HOME_PHONE = 294560642;
    const FIELD_CELL_PHONE = -1948415547;
    const FIELD_TIME_TO_CALL = -1508918620;
    const FIELD_PROFESSION = 2143048304;
    const FIELD_ADDRESS = 1900461824;
    const FIELD_CITY = 990720479;
    const FIELD_STATE = 662649501;
    const FIELD_COUNTRY = -288982366;
    const FIELD_ZIP_CODE = -126149953;
    const FIELD_LANGUAGES = 1626499111;
    const FIELD_OTHER_LANGUAGES = 1906119864;
    const FIELD_SPECIALTIES = 1919784;
    const FIELD_START_TIME = 1780164094;
    const FIELD_TOTAL_HOURS = 470237024;
    const FIELD_LEAD = 990984144;
    const FIELD_COMMENTS = -598926232;
    const FIELD_DELETED = 303649573;
    const FIELD_CREATED_ON = 428022794;
    const FIELD_UPDATED_ON = -1237121705;
    const FIELD_ATTACHMENTS = 0;
    const FIELD_LICENSES = 1;

    private static $PRIMARY_KEYS = array(self::FIELD_ID);
    private static $AUTOINCREMENT_FIELDS = array(self::FIELD_ID);
    private static $FIELD_NAMES = array(
        self::FIELD_ID => 'id',
        self::FIELD_EMAIL_ADDRESS => 'email_address',
        self::FIELD_PASSWORD => 'password',
        self::FIELD_FIRST_NAME => 'first_name',
        self::FIELD_MIDDLE_NAME => 'middle_name',
        self::FIELD_LAST_NAME => 'last_name',
        self::FIELD_HOME_PHONE => 'home_phone',
        self::FIELD_CELL_PHONE => 'cell_phone',
        self::FIELD_TIME_TO_CALL => 'time_to_call',
        self::FIELD_PROFESSION => 'profession',
        self::FIELD_ADDRESS => 'address',
        self::FIELD_CITY => 'city',
        self::FIELD_STATE => 'state',
        self::FIELD_COUNTRY => 'country',
        self::FIELD_ZIP_CODE => 'zip_code',
        self::FIELD_LANGUAGES => 'languages',
        self::FIELD_OTHER_LANGUAGES => 'other_languages',
        self::FIELD_SPECIALTIES => 'specialties',
        self::FIELD_START_TIME => 'start_time',
        self::FIELD_TOTAL_HOURS => 'total_hours',
        self::FIELD_LEAD => 'lead',
        self::FIELD_COMMENTS => 'comments',
        self::FIELD_DELETED => 'deleted',
        self::FIELD_CREATED_ON => 'created_on',
        self::FIELD_UPDATED_ON => 'updated_on');
    private static $PROPERTY_NAMES = array(
        self::FIELD_ID => 'id',
        self::FIELD_EMAIL_ADDRESS => 'emailAddress',
        self::FIELD_PASSWORD => 'password',
        self::FIELD_FIRST_NAME => 'firstName',
        self::FIELD_MIDDLE_NAME => 'middleName',
        self::FIELD_LAST_NAME => 'lastName',
        self::FIELD_HOME_PHONE => 'homePhone',
        self::FIELD_CELL_PHONE => 'cellPhone',
        self::FIELD_TIME_TO_CALL => 'timeToCall',
        self::FIELD_PROFESSION => 'profession',
        self::FIELD_ADDRESS => 'address',
        self::FIELD_CITY => 'city',
        self::FIELD_STATE => 'state',
        self::FIELD_COUNTRY => 'country',
        self::FIELD_ZIP_CODE => 'zipCode',
        self::FIELD_LANGUAGES => 'languages',
        self::FIELD_OTHER_LANGUAGES => 'otherLanguages',
        self::FIELD_SPECIALTIES => 'specialties',
        self::FIELD_START_TIME => 'startTime',
        self::FIELD_TOTAL_HOURS => 'totalHours',
        self::FIELD_LEAD => 'lead',
        self::FIELD_COMMENTS => 'comments',
        self::FIELD_DELETED => 'deleted',
        self::FIELD_CREATED_ON => 'createdOn',
        self::FIELD_UPDATED_ON => 'updatedOn',
        self::FIELD_ATTACHMENTS => 'attachments',
        self::FIELD_LICENSES => 'licenses');
    private static $DEFAULT_VALUES = array(
        self::FIELD_ID => null,
        self::FIELD_EMAIL_ADDRESS => '',
        self::FIELD_PASSWORD => '',
        self::FIELD_FIRST_NAME => '',
        self::FIELD_MIDDLE_NAME => null,
        self::FIELD_LAST_NAME => '',
        self::FIELD_HOME_PHONE => null,
        self::FIELD_CELL_PHONE => null,
        self::FIELD_TIME_TO_CALL => null,
        self::FIELD_PROFESSION => null,
        self::FIELD_ADDRESS => null,
        self::FIELD_CITY => null,
        self::FIELD_STATE => null,
        self::FIELD_COUNTRY => null,
        self::FIELD_ZIP_CODE => null,
        self::FIELD_LANGUAGES => null,
        self::FIELD_OTHER_LANGUAGES => null,
        self::FIELD_SPECIALTIES => null,
        self::FIELD_START_TIME => null,
        self::FIELD_TOTAL_HOURS => null,
        self::FIELD_LEAD => null,
        self::FIELD_COMMENTS => null,
        self::FIELD_DELETED => '0',
        self::FIELD_CREATED_ON => '2000-01-01 00:00:00',
        self::FIELD_UPDATED_ON => 'CURRENT_TIMESTAMP');
    private $id;
    private $emailAddress;
    private $password;
    private $firstName;
    private $middleName;
    private $lastName;
    private $homePhone;
    private $cellPhone;
    private $timeToCall;
    private $profession;
    private $address;
    private $city;
    private $state;
    private $country;
    private $zipCode;
    private $languages;
    private $otherLanguages;
    private $specialties;
    private $startTime;
    private $totalHours;
    private $lead;
    private $comments;
    private $deleted;
    private $createdOn;
    private $updatedOn;
    private $attachments = array();
    private $licenses = array();
    
    
    public function __construct() {
        $this->setCreatedOn(date('Y-m-d H:i:s'));
        $this->setUpdatedOn(date('Y-m-d H:i:s'));
    }

    /**
     * set value for id 
     *
     * type:INT UNSIGNED,size:10,default:null,primary,unique,autoincrement
     *
     * @param mixed $id
     * @return Profile
     */
    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    /**
     * get value for id 
     *
     * type:INT UNSIGNED,size:10,default:null,primary,unique,autoincrement
     *
     * @return mixed
     */
    public function getId() {
        return $this->id;
    }

    /**
     * set value for email_address 
     *
     * type:VARCHAR,size:80,default:null
     *
     * @param mixed $emailAddress
     * @return Profile
     */
    public function setEmailAddress($emailAddress) {
        $this->emailAddress = $emailAddress;
        return $this;
    }

    /**
     * get value for email_address 
     *
     * type:VARCHAR,size:80,default:null
     *
     * @return mixed
     */
    public function getEmailAddress() {
        return $this->emailAddress;
    }

    /**
     * set value for password 
     *
     * type:VARCHAR,size:255,default:null
     *
     * @param mixed $password
     * @return Profile
     */
    public function setPassword($password) {
        $this->password = $password;
        return $this;
    }

    /**
     * get value for password 
     *
     * type:VARCHAR,size:255,default:null
     *
     * @return mixed
     */
    public function getPassword() {
        return $this->password;
    }

    /**
     * set value for first_name 
     *
     * type:VARCHAR,size:40,default:null
     *
     * @param mixed $firstName
     * @return Profile
     */
    public function setFirstName($firstName) {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * get value for first_name 
     *
     * type:VARCHAR,size:40,default:null
     *
     * @return mixed
     */
    public function getFirstName() {
        return $this->firstName;
    }

    /**
     * set value for middle_name 
     *
     * type:VARCHAR,size:40,default:null,nullable
     *
     * @param mixed $middleName
     * @return Profile
     */
    public function setMiddleName($middleName) {
        $this->middleName = $middleName;
        return $this;
    }

    /**
     * get value for middle_name 
     *
     * type:VARCHAR,size:40,default:null,nullable
     *
     * @return mixed
     */
    public function getMiddleName() {
        return $this->middleName;
    }

    /**
     * set value for last_name 
     *
     * type:VARCHAR,size:40,default:null
     *
     * @param mixed $lastName
     * @return Profile
     */
    public function setLastName($lastName) {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * get value for last_name 
     *
     * type:VARCHAR,size:40,default:null
     *
     * @return mixed
     */
    public function getLastName() {
        return $this->lastName;
    }

    /**
     * set value for home_phone 
     *
     * type:VARCHAR,size:25,default:null,nullable
     *
     * @param mixed $homePhone
     * @return Profile
     */
    public function setHomePhone($homePhone) {
        $this->homePhone = $homePhone;
        return $this;
    }

    /**
     * get value for home_phone 
     *
     * type:VARCHAR,size:25,default:null,nullable
     *
     * @return mixed
     */
    public function getHomePhone() {
        return $this->homePhone;
    }

    /**
     * set value for cell_phone 
     *
     * type:VARCHAR,size:25,default:null,nullable
     *
     * @param mixed $cellPhone
     * @return Profile
     */
    public function setCellPhone($cellPhone) {
        $this->cellPhone = $cellPhone;
        return $this;
    }

    /**
     * get value for cell_phone 
     *
     * type:VARCHAR,size:25,default:null,nullable
     *
     * @return mixed
     */
    public function getCellPhone() {
        return $this->cellPhone;
    }

    /**
     * set value for time_to_call 
     *
     * type:VARCHAR,size:40,default:null,nullable
     *
     * @param mixed $timeToCall
     * @return Profile
     */
    public function setTimeToCall($timeToCall) {
        $this->timeToCall = $timeToCall;
        return $this;
    }

    /**
     * get value for time_to_call 
     *
     * type:VARCHAR,size:40,default:null,nullable
     *
     * @return mixed
     */
    public function getTimeToCall() {
        return $this->timeToCall;
    }

    /**
     * set value for profession 
     *
     * type:VARCHAR,size:40,default:null,nullable
     *
     * @param mixed $profession
     * @return Profile
     */
    public function setProfession($profession) {
        $this->profession = $profession;
        return $this;
    }

    /**
     * get value for profession 
     *
     * type:VARCHAR,size:40,default:null,nullable
     *
     * @return mixed
     */
    public function getProfession() {
        return $this->profession;
    }

    /**
     * set value for address 
     *
     * type:VARCHAR,size:80,default:null,nullable
     *
     * @param mixed $address
     * @return Profile
     */
    public function setAddress($address) {
        $this->address = $address;
        return $this;
    }

    /**
     * get value for address 
     *
     * type:VARCHAR,size:80,default:null,nullable
     *
     * @return mixed
     */
    public function getAddress() {
        return $this->address;
    }

    /**
     * set value for city 
     *
     * type:VARCHAR,size:50,default:null,nullable
     *
     * @param mixed $city
     * @return Profile
     */
    public function setCity($city) {
        $this->city = $city;
        return $this;
    }

    /**
     * get value for city 
     *
     * type:VARCHAR,size:50,default:null,nullable
     *
     * @return mixed
     */
    public function getCity() {
        return $this->city;
    }

    /**
     * set value for state 
     *
     * type:VARCHAR,size:50,default:null,nullable
     *
     * @param mixed $state
     * @return Profile
     */
    public function setState($state) {
        $this->state = $state;
        return $this;
    }

    /**
     * get value for state 
     *
     * type:VARCHAR,size:50,default:null,nullable
     *
     * @return mixed
     */
    public function getState() {
        return $this->state;
    }

    /**
     * set value for country 
     *
     * type:VARCHAR,size:40,default:null,nullable
     *
     * @param mixed $country
     * @return Profile
     */
    public function setCountry($country) {
        $this->country = $country;
        return $this;
    }

    /**
     * get value for country 
     *
     * type:VARCHAR,size:40,default:null,nullable
     *
     * @return mixed
     */
    public function getCountry() {
        return $this->country;
    }

    /**
     * set value for zip_code 
     *
     * type:VARCHAR,size:20,default:null,nullable
     *
     * @param mixed $zipCode
     * @return Profile
     */
    public function setZipCode($zipCode) {
        $this->zipCode = $zipCode;
        return $this;
    }

    /**
     * get value for zip_code 
     *
     * type:VARCHAR,size:20,default:null,nullable
     *
     * @return mixed
     */
    public function getZipCode() {
        return $this->zipCode;
    }

    /**
     * set value for languages 
     *
     * type:VARCHAR,size:80,default:null,nullable
     *
     * @param mixed $languages
     * @return Profile
     */
    public function setLanguages($languages) {
        $this->languages = $languages;
        return $this;
    }

    /**
     * get value for languages 
     *
     * type:VARCHAR,size:80,default:null,nullable
     *
     * @return mixed
     */
    public function getLanguages() {
        return $this->languages;
    }

    /**
     * set value for other_languages 
     *
     * type:VARCHAR,size:80,default:null,nullable
     *
     * @param mixed $otherLanguages
     * @return Profile
     */
    public function setOtherLanguages($otherLanguages) {
        $this->otherLanguages = $otherLanguages;
        return $this;
    }

    /**
     * get value for other_languages 
     *
     * type:VARCHAR,size:80,default:null,nullable
     *
     * @return mixed
     */
    public function getOtherLanguages() {
        return $this->otherLanguages;
    }

    /**
     * set value for specialties 
     *
     * type:VARCHAR,size:80,default:null,nullable
     *
     * @param mixed $specialties
     * @return Profile
     */
    public function setSpecialties($specialties) {
        $this->specialties = $specialties;
        return $this;
    }

    /**
     * get value for specialties 
     *
     * type:VARCHAR,size:80,default:null,nullable
     *
     * @return mixed
     */
    public function getSpecialties() {
        return $this->specialties;
    }

    /**
     * set value for start_time 
     *
     * type:VARCHAR,size:40,default:null,nullable
     *
     * @param mixed $startTime
     * @return Profile
     */
    public function setStartTime($startTime) {
        $this->startTime = $startTime;
        return $this;
    }

    /**
     * get value for start_time 
     *
     * type:VARCHAR,size:40,default:null,nullable
     *
     * @return mixed
     */
    public function getStartTime() {
        return $this->startTime;
    }

    /**
     * set value for total_hours 
     *
     * type:VARCHAR,size:40,default:null,nullable
     *
     * @param mixed $totalHours
     * @return Profile
     */
    public function setTotalHours($totalHours) {
        $this->totalHours = $totalHours;
        return $this;
    }

    /**
     * get value for total_hours 
     *
     * type:VARCHAR,size:40,default:null,nullable
     *
     * @return mixed
     */
    public function getTotalHours() {
        return $this->totalHours;
    }

    /**
     * set value for lead 
     *
     * type:VARCHAR,size:40,default:null,nullable
     *
     * @param mixed $lead
     * @return Profile
     */
    public function setLead($lead) {
        $this->lead = $lead;
        return $this;
    }

    /**
     * get value for lead 
     *
     * type:VARCHAR,size:40,default:null,nullable
     *
     * @return mixed
     */
    public function getLead() {
        return $this->lead;
    }

    /**
     * set value for comments 
     *
     * type:VARCHAR,size:255,default:null,nullable
     *
     * @param mixed $comments
     * @return Profile
     */
    public function setComments($comments) {
        $this->comments = $comments;
        return $this;
    }

    /**
     * get value for comments 
     *
     * type:VARCHAR,size:255,default:null,nullable
     *
     * @return mixed
     */
    public function getComments() {
        return $this->comments;
    }

    /**
     * set value for deleted 
     *
     * type:BIT,size:0,default:0
     *
     * @param mixed $deleted
     * @return Profile
     */
    public function setDeleted($deleted) {
        $this->deleted = $deleted;
        return $this;
    }

    /**
     * get value for deleted 
     *
     * type:BIT,size:0,default:0
     *
     * @return mixed
     */
    public function getDeleted() {
        return $this->deleted;
    }

    /**
     * set value for created_on 
     *
     * type:TIMESTAMP,size:19,default:2000-01-01 00:00:00
     *
     * @param mixed $createdOn
     * @return Profile
     */
    public function setCreatedOn($createdOn) {
        $this->createdOn = $createdOn;
        return $this;
    }

    /**
     * get value for created_on 
     *
     * type:TIMESTAMP,size:19,default:2000-01-01 00:00:00
     *
     * @return mixed
     */
    public function getCreatedOn() {
        return $this->createdOn;
    }

    /**
     * set value for updated_on 
     *
     * type:TIMESTAMP,size:19,default:CURRENT_TIMESTAMP
     *
     * @param mixed $updatedOn
     * @return Profile
     */
    public function setUpdatedOn($updatedOn) {
        $this->updatedOn = $updatedOn;
        return $this;
    }

    /**
     * get value for updated_on 
     *
     * type:TIMESTAMP,size:19,default:CURRENT_TIMESTAMP
     *
     * @return mixed
     */
    public function getUpdatedOn() {
        return $this->updatedOn;
    }
    
    public function setAttachments($attachments) {
        $this->attachments = $attachments;
        return $this;
    }
    
    public function addAttachment($attachment) {
        $this->attachments[] = $attachment;
    }


    public function getAttachments() {
        return $this->attachments;
    }
    
    public function setLicenses($licenses) {
        $this->licenses = $licenses;
        return $this;
    }
    
    public function addLicense($licenses) {
        $this->licenses[] = $licenses;
    }


    public function getLicenses() {
        return $this->licenses;
    }

    /**
     * Get table name
     *
     * @return string
     */
    public static function getTableName() {
        return self::SQL_TABLE_NAME;
    }

    /**
     * Get array with field id as index and field name as value
     *
     * @return array
     */
    public static function getFieldNames() {
        return self::$FIELD_NAMES;
    }

    /**
     * Get array with field id as index and property name as value
     *
     * @return array
     */
    public static function getPropertyNames() {
        return self::$PROPERTY_NAMES;
    }

    /**
     * get the field name for the passed field id.
     *
     * @param int $fieldId
     * @param bool $fullyQualifiedName true if field name should be qualified by table name
     * @return string field name for the passed field id, null if the field doesn't exist
     */
    public static function getFieldNameByFieldId($fieldId, $fullyQualifiedName = true) {
        if (!array_key_exists($fieldId, self::$FIELD_NAMES)) {
            return null;
        }
        $fieldName = self::SQL_IDENTIFIER_QUOTE . self::$FIELD_NAMES[$fieldId] . self::SQL_IDENTIFIER_QUOTE;
        if ($fullyQualifiedName) {
            return self::SQL_IDENTIFIER_QUOTE . self::SQL_TABLE_NAME . self::SQL_IDENTIFIER_QUOTE . '.' . $fieldName;
        }
        return $fieldName;
    }

    /**
     * Get array with field ids of identifiers
     *
     * @return array
     */
    public static function getIdentifierFields() {
        return self::$PRIMARY_KEYS;
    }

    /**
     * Get array with field ids of autoincrement fields
     *
     * @return array
     */
    public static function getAutoincrementFields() {
        return self::$AUTOINCREMENT_FIELDS;
    }

    /**
     * Get array with field id as index and property type as value
     *
     * @return array
     */
    public static function getPropertyTypes() {
        return self::$PROPERTY_TYPES;
    }

    /**
     * Get array with field id as index and field type as value
     *
     * @return array
     */
    public static function getFieldTypes() {
        return self::$FIELD_TYPES;
    }

    /**
     * Assign default values according to table
     * 
     */
    public function assignDefaultValues() {
        $this->assignByArray(self::$DEFAULT_VALUES);
    }

    /**
     * return hash with the field name as index and the field value as value.
     *
     * @return array
     */
    public function toHash() {
        $array = $this->toArray();
        $hash = array();
        foreach ($array as $fieldId => $value) {
            $hash[self::$FIELD_NAMES[$fieldId]] = $value;
        }
        return $hash;
    }

    /**
     * return array with the field id as index and the field value as value.
     *
     * @return array
     */
    public function toArray() {
        $attachmentsArr = array();
        foreach ($this->getAttachments() as $attachment) {
            $attachmentsArr[] = $attachment->toArray();
        }
        $licensesArr = array();
        foreach ($this->getLicenses() as $license) {
            $licensesArr[] = $license->toArray();
        }
        
        return array(
            self::$PROPERTY_NAMES[self::FIELD_ID] => $this->getId(),
            self::$PROPERTY_NAMES[self::FIELD_EMAIL_ADDRESS] => $this->getEmailAddress(),
            //self::$PROPERTY_NAMES[self::FIELD_PASSWORD] => $this->getPassword(),
            self::$PROPERTY_NAMES[self::FIELD_FIRST_NAME] => $this->getFirstName(),
            self::$PROPERTY_NAMES[self::FIELD_MIDDLE_NAME] => $this->getMiddleName(),
            self::$PROPERTY_NAMES[self::FIELD_LAST_NAME] => $this->getLastName(),
            self::$PROPERTY_NAMES[self::FIELD_HOME_PHONE] => $this->getHomePhone(),
            self::$PROPERTY_NAMES[self::FIELD_CELL_PHONE] => $this->getCellPhone(),
            self::$PROPERTY_NAMES[self::FIELD_TIME_TO_CALL] => $this->getTimeToCall(),
            self::$PROPERTY_NAMES[self::FIELD_PROFESSION] => $this->getProfession(),
            self::$PROPERTY_NAMES[self::FIELD_ADDRESS] => $this->getAddress(),
            self::$PROPERTY_NAMES[self::FIELD_CITY] => $this->getCity(),
            self::$PROPERTY_NAMES[self::FIELD_STATE] => $this->getState(),
            self::$PROPERTY_NAMES[self::FIELD_COUNTRY] => $this->getCountry(),
            self::$PROPERTY_NAMES[self::FIELD_ZIP_CODE] => $this->getZipCode(),
            self::$PROPERTY_NAMES[self::FIELD_LANGUAGES] => $this->getLanguages(),
            self::$PROPERTY_NAMES[self::FIELD_OTHER_LANGUAGES] => $this->getOtherLanguages(),
            self::$PROPERTY_NAMES[self::FIELD_SPECIALTIES] => $this->getSpecialties(),
            self::$PROPERTY_NAMES[self::FIELD_START_TIME] => $this->getStartTime(),
            self::$PROPERTY_NAMES[self::FIELD_TOTAL_HOURS] => $this->getTotalHours(),
            self::$PROPERTY_NAMES[self::FIELD_LEAD] => $this->getLead(),
            self::$PROPERTY_NAMES[self::FIELD_COMMENTS] => $this->getComments(),
            self::$PROPERTY_NAMES[self::FIELD_DELETED] => $this->getDeleted(),
            self::$PROPERTY_NAMES[self::FIELD_CREATED_ON] => $this->getCreatedOn(),
            self::$PROPERTY_NAMES[self::FIELD_UPDATED_ON] => $this->getUpdatedOn(),
            self::$PROPERTY_NAMES[self::FIELD_ATTACHMENTS] => $attachmentsArr,
            self::$PROPERTY_NAMES[self::FIELD_LICENSES] => $licensesArr);
    }

    /**
     * return array with the field id as index and the field value as value for the identifier fields.
     *
     * @return array
     */
    public function getPrimaryKeyValues() {
        return array(
            self::FIELD_ID => $this->getId());
    }

    /**
     * Assign values from array with the field id as index and the value as value
     *
     * @param array $array
     */
    public function assignByArray($array) {
        $result = array();
        foreach ($array as $fieldId => $value) {
            $result[self::$FIELD_NAMES[$fieldId]] = $value;
        }
        $this->assignByHash($result);
    }

    /**
     * Assign values from hash where the indexes match the tables field names
     *
     * @param array $result
     */
    public function assignByHash($result) {
        $this->setId($result['id']);
        $this->setEmailAddress($result['email_address']);
        $this->setPassword($result['password']);
        $this->setFirstName($result['first_name']);
        $this->setMiddleName($result['middle_name']);
        $this->setLastName($result['last_name']);
        $this->setHomePhone($result['home_phone']);
        $this->setCellPhone($result['cell_phone']);
        $this->setTimeToCall($result['time_to_call']);
        $this->setProfession($result['profession']);
        $this->setAddress($result['address']);
        $this->setCity($result['city']);
        $this->setState($result['state']);
        $this->setCountry($result['country']);
        $this->setZipCode($result['zip_code']);
        $this->setLanguages($result['languages']);
        $this->setOtherLanguages($result['other_languages']);
        $this->setSpecialties($result['specialties']);
        $this->setStartTime($result['start_time']);
        $this->setTotalHours($result['total_hours']);
        $this->setLead($result['lead']);
        $this->setComments($result['comments']);
        $this->setDeleted($result['deleted']);
        $this->setCreatedOn($result['created_on']);
        $this->setUpdatedOn($result['updated_on']);
    }

    /**
     * Bind all values to statement
     *
     * @param PDOStatement $stmt
     */
    protected function bindValues(PDOStatement &$stmt) {
        $stmt->bindValue(1, $this->getId());
        $stmt->bindValue(2, $this->getEmailAddress());
        $stmt->bindValue(3, $this->getPassword());
        $stmt->bindValue(4, $this->getFirstName());
        $stmt->bindValue(5, $this->getMiddleName());
        $stmt->bindValue(6, $this->getLastName());
        $stmt->bindValue(7, $this->getHomePhone());
        $stmt->bindValue(8, $this->getCellPhone());
        $stmt->bindValue(9, $this->getTimeToCall());
        $stmt->bindValue(10, $this->getProfession());
        $stmt->bindValue(11, $this->getAddress());
        $stmt->bindValue(12, $this->getCity());
        $stmt->bindValue(13, $this->getState());
        $stmt->bindValue(14, $this->getCountry());
        $stmt->bindValue(15, $this->getZipCode());
        $stmt->bindValue(16, $this->getLanguages());
        $stmt->bindValue(17, $this->getOtherLanguages());
        $stmt->bindValue(18, $this->getSpecialties());
        $stmt->bindValue(19, $this->getStartTime());
        $stmt->bindValue(20, $this->getTotalHours());
        $stmt->bindValue(21, $this->getLead());
        $stmt->bindValue(22, $this->getComments());
        $stmt->bindValue(23, $this->getDeleted());
        $stmt->bindValue(24, $this->getCreatedOn());
        $stmt->bindValue(25, $this->getUpdatedOn());
    }

}
