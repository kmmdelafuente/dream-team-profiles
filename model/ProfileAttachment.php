<?php

/**
 * 
 *
 * @version 1.107
 * @package entity
 */
class ProfileAttachment {

    const SQL_IDENTIFIER_QUOTE = '`';
    const SQL_TABLE_NAME = 'dt_profile_attachment';
    const FIELD_ID = 1291184961;
    const FIELD_PROFILE_ID = -1599638633;
    const FIELD_TYPE = -416449568;
    const FIELD_FILENAME = 1459125165;
    const FIELD_FILETYPE = 1459125164;
    const FIELD_CONTENTS = 1459125166;
    const FIELD_COMMENTS = 1591478170;
    const FIELD_UPLOADED_ON = -1452120904;
    const FIELD_DELETED = -179881549;
    const FIELD_CREATED_ON = 872678076;
    const FIELD_UPDATED_ON = -792466423;

    private static $PRIMARY_KEYS = array(self::FIELD_ID);
    private static $AUTOINCREMENT_FIELDS = array(self::FIELD_ID);
    private static $FIELD_NAMES = array(
        self::FIELD_ID => 'id',
        self::FIELD_PROFILE_ID => 'profile_id',
        self::FIELD_TYPE => 'type',
        self::FIELD_FILENAME => 'filename',
        self::FIELD_FILETYPE => 'filetype',
        self::FIELD_CONTENTS => 'contents',
        self::FIELD_COMMENTS => 'comments',
        self::FIELD_UPLOADED_ON => 'uploaded_on',
        self::FIELD_DELETED => 'deleted',
        self::FIELD_CREATED_ON => 'created_on',
        self::FIELD_UPDATED_ON => 'updated_on');
    private static $PROPERTY_NAMES = array(
        self::FIELD_ID => 'id',
        self::FIELD_PROFILE_ID => 'profileId',
        self::FIELD_TYPE => 'type',
        self::FIELD_FILENAME => 'filename',
        self::FIELD_FILETYPE => 'filetype',
        self::FIELD_CONTENTS => 'contents',
        self::FIELD_COMMENTS => 'comments',
        self::FIELD_UPLOADED_ON => 'uploadedOn',
        self::FIELD_DELETED => 'deleted',
        self::FIELD_CREATED_ON => 'createdOn',
        self::FIELD_UPDATED_ON => 'updatedOn');
    private static $DEFAULT_VALUES = array(
        self::FIELD_ID => null,
        self::FIELD_PROFILE_ID => 0,
        self::FIELD_TYPE => '',
        self::FIELD_FILENAME => '',
        self::FIELD_FILETYPE => '',
        self::FIELD_CONTENTS => null,
        self::FIELD_COMMENTS => null,
        self::FIELD_UPLOADED_ON => null,
        self::FIELD_DELETED => '0',
        self::FIELD_CREATED_ON => '2000-01-01 00:00:00',
        self::FIELD_UPDATED_ON => 'CURRENT_TIMESTAMP');
    private $id;
    private $profileId;
    private $type;
    private $filename;
    private $filetype;
    private $contents;
    private $comments;
    private $uploadedOn;
    private $deleted;
    private $createdOn;
    private $updatedOn;

    public function __construct() {
        $this->setCreatedOn(date('Y-m-d H:i:s'));
        $this->setUpdatedOn(date('Y-m-d H:i:s'));
    }

    /**
     * set value for id 
     *
     * type:INT UNSIGNED,size:10,default:null,primary,unique,autoincrement
     *
     * @param mixed $id
     */
    public function setId($id) {
        $this->id = $id;
    }

    /**
     * get value for id 
     *
     * type:INT UNSIGNED,size:10,default:null,primary,unique,autoincrement
     *
     * @return mixed
     */
    public function getId() {
        return $this->id;
    }

    /**
     * set value for profile_id 
     *
     * type:INT UNSIGNED,size:10,default:null,index
     *
     * @param mixed $profileId
     */
    public function setProfileId($profileId) {
        $this->profileId = $profileId;
    }

    /**
     * get value for profile_id 
     *
     * type:INT UNSIGNED,size:10,default:null,index
     *
     * @return mixed
     */
    public function getProfileId() {
        return $this->profileId;
    }

    /**
     * set value for type 
     *
     * type:ENUM,size:11,default:null
     *
     * @param mixed $type
     */
    public function setType($type) {
        $this->type = $type;
    }

    /**
     * get value for type 
     *
     * type:ENUM,size:11,default:null
     *
     * @return mixed
     */
    public function getType() {
        return $this->type;
    }

    /**
     * set value for filename 
     *
     * type:VARCHAR,size:80,default:null
     *
     * @param mixed $filename
     */
    public function setFilename($filename) {
        $this->filename = $filename;
    }

    /**
     * get value for filename 
     *
     * type:VARCHAR,size:80,default:null
     *
     * @return mixed
     */
    public function getFilename() {
        return $this->filename;
    }

    /**
     * set value for filetype 
     *
     * type:VARCHAR,size:80,default:null
     *
     * @param mixed $filetype
     */
    public function setFiletype($filetype) {
        $this->filetype = $filetype;
    }

    /**
     * get value for filetype 
     *
     * type:VARCHAR,size:80,default:null
     *
     * @return mixed
     */
    public function getFiletype() {
        return $this->filetype;
    }

    /**
     * set value for contents 
     *
     * type:VARCHAR,size:80,default:null
     *
     * @param mixed $contents
     */
    public function setContents($contents) {
        $this->contents = $contents;
    }

    /**
     * get value for contents 
     *
     * type:VARCHAR,size:80,default:null
     *
     * @return mixed
     */
    public function getContents() {
        return $this->contents;
    }

    /**
     * set value for comments 
     *
     * type:VARCHAR,size:255,default:null,nullable
     *
     * @param mixed $comments
     */
    public function setComments($comments) {
        $this->comments = $comments;
    }

    /**
     * get value for comments 
     *
     * type:VARCHAR,size:255,default:null,nullable
     *
     * @return mixed
     */
    public function getComments() {
        return $this->comments;
    }

    /**
     * set value for uploaded_on 
     *
     * type:DATE,size:10,default:null,nullable
     *
     * @param mixed $uploadedOn
     */
    public function setUploadedOn($uploadedOn) {
        $this->uploadedOn = $uploadedOn;
    }

    /**
     * get value for uploaded_on 
     *
     * type:DATE,size:10,default:null,nullable
     *
     * @return mixed
     */
    public function getUploadedOn() {
        return $this->uploadedOn;
    }

    /**
     * set value for deleted 
     *
     * type:BIT,size:0,default:0
     *
     * @param mixed $deleted
     */
    public function setDeleted($deleted) {
        $this->deleted = $deleted;
    }

    /**
     * get value for deleted 
     *
     * type:BIT,size:0,default:0
     *
     * @return mixed
     */
    public function getDeleted() {
        return $this->deleted;
    }

    /**
     * set value for created_on 
     *
     * type:TIMESTAMP,size:19,default:2000-01-01 00:00:00
     *
     * @param mixed $createdOn
     */
    public function setCreatedOn($createdOn) {
        $this->createdOn = $createdOn;
    }

    /**
     * get value for created_on 
     *
     * type:TIMESTAMP,size:19,default:2000-01-01 00:00:00
     *
     * @return mixed
     */
    public function getCreatedOn() {
        return $this->createdOn;
    }

    /**
     * set value for updated_on 
     *
     * type:TIMESTAMP,size:19,default:CURRENT_TIMESTAMP
     *
     * @param mixed $updatedOn
     */
    public function setUpdatedOn($updatedOn) {
        $this->updatedOn = $updatedOn;
    }

    /**
     * get value for updated_on 
     *
     * type:TIMESTAMP,size:19,default:CURRENT_TIMESTAMP
     *
     * @return mixed
     */
    public function getUpdatedOn() {
        return $this->updatedOn;
    }

    /**
     * Get table name
     *
     * @return string
     */
    public static function getTableName() {
        return self::SQL_TABLE_NAME;
    }

    /**
     * Get array with field id as index and field name as value
     *
     * @return array
     */
    public static function getFieldNames() {
        return self::$FIELD_NAMES;
    }

    /**
     * Get array with field id as index and property name as value
     *
     * @return array
     */
    public static function getPropertyNames() {
        return self::$PROPERTY_NAMES;
    }

    /**
     * get the field name for the passed field id.
     *
     * @param int $fieldId
     * @param bool $fullyQualifiedName true if field name should be qualified by table name
     * @return string field name for the passed field id, null if the field doesn't exist
     */
    public static function getFieldNameByFieldId($fieldId, $fullyQualifiedName = true) {
        if (!array_key_exists($fieldId, self::$FIELD_NAMES)) {
            return null;
        }
        $fieldName = self::SQL_IDENTIFIER_QUOTE . self::$FIELD_NAMES[$fieldId] . self::SQL_IDENTIFIER_QUOTE;
        if ($fullyQualifiedName) {
            return self::SQL_IDENTIFIER_QUOTE . self::SQL_TABLE_NAME . self::SQL_IDENTIFIER_QUOTE . '.' . $fieldName;
        }
        return $fieldName;
    }

    /**
     * Get array with field ids of identifiers
     *
     * @return array
     */
    public static function getIdentifierFields() {
        return self::$PRIMARY_KEYS;
    }

    /**
     * Get array with field ids of autoincrement fields
     *
     * @return array
     */
    public static function getAutoincrementFields() {
        return self::$AUTOINCREMENT_FIELDS;
    }

    /**
     * Get array with field id as index and property type as value
     *
     * @return array
     */
    public static function getPropertyTypes() {
        return self::$PROPERTY_TYPES;
    }

    /**
     * Get array with field id as index and field type as value
     *
     * @return array
     */
    public static function getFieldTypes() {
        return self::$FIELD_TYPES;
    }

    /**
     * Assign default values according to table
     * 
     */
    public function assignDefaultValues() {
        $this->assignByArray(self::$DEFAULT_VALUES);
    }

    /**
     * return hash with the field name as index and the field value as value.
     *
     * @return array
     */
    public function toHash() {
        $array = $this->toArray();
        $hash = array();
        foreach ($array as $fieldId => $value) {
            $hash[self::$FIELD_NAMES[$fieldId]] = $value;
        }
        return $hash;
    }

    /**
     * return array with the field id as index and the field value as value.
     *
     * @return array
     */
    public function toArray() {
        return array(
            self::$PROPERTY_NAMES[self::FIELD_ID] => $this->getId(),
            self::$PROPERTY_NAMES[self::FIELD_PROFILE_ID] => $this->getProfileId(),
            self::$PROPERTY_NAMES[self::FIELD_TYPE] => $this->getType(),
            self::$PROPERTY_NAMES[self::FIELD_FILENAME] => $this->getFilename(),
            self::$PROPERTY_NAMES[self::FIELD_FILETYPE] => $this->getFiletype(),
            self::$PROPERTY_NAMES[self::FIELD_CONTENTS] => $this->getContents(),
            self::$PROPERTY_NAMES[self::FIELD_COMMENTS] => $this->getComments(),
            self::$PROPERTY_NAMES[self::FIELD_UPLOADED_ON] => $this->getUploadedOn(),
            self::$PROPERTY_NAMES[self::FIELD_DELETED] => $this->getDeleted(),
            self::$PROPERTY_NAMES[self::FIELD_CREATED_ON] => $this->getCreatedOn(),
            self::$PROPERTY_NAMES[self::FIELD_UPDATED_ON] => $this->getUpdatedOn());
    }

    /**
     * return array with the field id as index and the field value as value for the identifier fields.
     *
     * @return array
     */
    public function getPrimaryKeyValues() {
        return array(
            self::FIELD_ID => $this->getId());
    }

    /**
     * Assign values from array with the field id as index and the value as value
     *
     * @param array $array
     */
    public function assignByArray($array) {
        $result = array();
        foreach ($array as $fieldId => $value) {
            $result[self::$FIELD_NAMES[$fieldId]] = $value;
        }
        $this->assignByHash($result);
    }

    /**
     * Assign values from hash where the indexes match the tables field names
     *
     * @param array $result
     */
    public function assignByHash($result) {
        $this->setId($result['id']);
        $this->setProfileId($result['profile_id']);
        $this->setType($result['type']);
        $this->setFilename($result['filename']);
        $this->setFiletype($result['filetype']);
        $this->setContents($result['contents']);
        $this->setComments($result['comments']);
        $this->setUploadedOn($result['uploaded_on']);
        $this->setDeleted($result['deleted']);
        $this->setCreatedOn($result['created_on']);
        $this->setUpdatedOn($result['updated_on']);
    }

    /**
     * Bind all values to statement
     *
     * @param PDOStatement $stmt
     */
    protected function bindValues(PDOStatement &$stmt) {
        $stmt->bindValue(1, $this->getId());
        $stmt->bindValue(2, $this->getProfileId());
        $stmt->bindValue(3, $this->getType());
        $stmt->bindValue(4, $this->getFilename());
        $stmt->bindValue(5, $this->getComments());
        $stmt->bindValue(6, $this->getUploadedOn());
        $stmt->bindValue(7, $this->getDeleted());
        $stmt->bindValue(8, $this->getCreatedOn());
        $stmt->bindValue(9, $this->getUpdatedOn());
        $stmt->bindValue(10, $this->getContents());
        $stmt->bindValue(11, $this->getFiletype());
    }

}
