<?php

/**
 * 
 *
 * @version 1.107
 * @package entity
 */
class ProfileLicense {

    private static $CLASS_NAME = 'ProfileLicense';

    const SQL_IDENTIFIER_QUOTE = '`';
    const SQL_TABLE_NAME = 'dt_profile_license';
    const SQL_INSERT = 'INSERT INTO `dt_profile_license` (`id`,`profile_id`,`license_id`,`comments`,`reference_number`,`expiry_date`,`deleted`,`created_on`,`updated_on`) VALUES (?,?,?,?,?,?,?,?,?)';
    const SQL_INSERT_AUTOINCREMENT = 'INSERT INTO `dt_profile_license` (`profile_id`,`license_id`,`comments`,`reference_number`,`expiry_date`,`deleted`,`created_on`,`updated_on`) VALUES (?,?,?,?,?,?,?,?)';
    const SQL_UPDATE = 'UPDATE `dt_profile_license` SET `id`=?,`profile_id`=?,`license_id`=?,`comments`=?,`reference_number`=?,`expiry_date`=?,`deleted`=?,`created_on`=?,`updated_on`=? WHERE `id`=?';
    const SQL_SELECT_PK = 'SELECT * FROM `dt_profile_license` WHERE `id`=?';
    const SQL_DELETE_PK = 'DELETE FROM `dt_profile_license` WHERE `id`=?';
    const FIELD_ID = -204150195;
    const FIELD_PROFILE_ID = -1488148829;
    const FIELD_LICENSE_ID = -1791654645;
    const FIELD_COMMENTS = -1224045146;
    const FIELD_REFERENCE_NUMBER = 2147073135;
    const FIELD_EXPIRY_DATE = 1409928776;
    const FIELD_DELETED = 560579111;
    const FIELD_CREATED_ON = 984167880;
    const FIELD_UPDATED_ON = -680976619;

    private static $PRIMARY_KEYS = array(self::FIELD_ID);
    private static $AUTOINCREMENT_FIELDS = array(self::FIELD_ID);
    private static $FIELD_NAMES = array(
        self::FIELD_ID => 'id',
        self::FIELD_PROFILE_ID => 'profile_id',
        self::FIELD_LICENSE_ID => 'license_id',
        self::FIELD_COMMENTS => 'comments',
        self::FIELD_REFERENCE_NUMBER => 'reference_number',
        self::FIELD_EXPIRY_DATE => 'expiry_date',
        self::FIELD_DELETED => 'deleted',
        self::FIELD_CREATED_ON => 'created_on',
        self::FIELD_UPDATED_ON => 'updated_on');
    private static $PROPERTY_NAMES = array(
        self::FIELD_ID => 'id',
        self::FIELD_PROFILE_ID => 'profileId',
        self::FIELD_LICENSE_ID => 'licenseId',
        self::FIELD_COMMENTS => 'comments',
        self::FIELD_REFERENCE_NUMBER => 'referenceNumber',
        self::FIELD_EXPIRY_DATE => 'expiryDate',
        self::FIELD_DELETED => 'deleted',
        self::FIELD_CREATED_ON => 'createdOn',
        self::FIELD_UPDATED_ON => 'updatedOn');
    private static $DEFAULT_VALUES = array(
        self::FIELD_ID => null,
        self::FIELD_PROFILE_ID => 0,
        self::FIELD_LICENSE_ID => 0,
        self::FIELD_COMMENTS => null,
        self::FIELD_REFERENCE_NUMBER => null,
        self::FIELD_EXPIRY_DATE => null,
        self::FIELD_DELETED => '0',
        self::FIELD_CREATED_ON => '0000-00-00 00:00:00',
        self::FIELD_UPDATED_ON => 'CURRENT_TIMESTAMP');
    private $id;
    private $profileId;
    private $licenseId;
    private $comments;
    private $referenceNumber;
    private $expiryDate;
    private $deleted;
    private $createdOn;
    private $updatedOn;

    /**
     * set value for id 
     *
     * type:INT UNSIGNED,size:10,default:null,primary,unique,autoincrement
     *
     * @param mixed $id
     */
    public function setId($id) {
        $this->id = $id;
    }

    /**
     * get value for id 
     *
     * type:INT UNSIGNED,size:10,default:null,primary,unique,autoincrement
     *
     * @return mixed
     */
    public function getId() {
        return $this->id;
    }

    /**
     * set value for profile_id 
     *
     * type:INT UNSIGNED,size:10,default:null,index
     *
     * @param mixed $profileId
     */
    public function setProfileId($profileId) {
        $this->profileId = $profileId;
    }

    /**
     * get value for profile_id 
     *
     * type:INT UNSIGNED,size:10,default:null,index
     *
     * @return mixed
     */
    public function getProfileId() {
        return $this->profileId;
    }

    /**
     * set value for license_id 
     *
     * type:INT UNSIGNED,size:10,default:null,index
     *
     * @param mixed $licenseId
     */
    public function setLicenseId($licenseId) {
        $this->licenseId = $licenseId;
    }

    /**
     * get value for license_id 
     *
     * type:INT UNSIGNED,size:10,default:null,index
     *
     * @return mixed
     */
    public function getLicenseId() {
        return $this->licenseId;
    }

    /**
     * set value for comments 
     *
     * type:VARCHAR,size:255,default:null,nullable
     *
     * @param mixed $comments
     */
    public function setComments($comments) {
        $this->comments = $comments;
    }

    /**
     * get value for comments 
     *
     * type:VARCHAR,size:255,default:null,nullable
     *
     * @return mixed
     */
    public function getComments() {
        return $this->comments;
    }

    /**
     * set value for reference_number 
     *
     * type:VARCHAR,size:50,default:null,nullable
     *
     * @param mixed $referenceNumber
     */
    public function setReferenceNumber($referenceNumber) {
        $this->referenceNumber = $referenceNumber;
    }

    /**
     * get value for reference_number 
     *
     * type:VARCHAR,size:50,default:null,nullable
     *
     * @return mixed
     */
    public function getReferenceNumber() {
        return $this->referenceNumber;
    }

    /**
     * set value for expiry_date 
     *
     * type:DATE,size:10,default:null,nullable
     *
     * @param mixed $expiryDate
     */
    public function setExpiryDate($expiryDate) {
        $this->expiryDate = $expiryDate;
    }

    /**
     * get value for expiry_date 
     *
     * type:DATE,size:10,default:null,nullable
     *
     * @return mixed
     */
    public function getExpiryDate() {
        return $this->expiryDate;
    }

    /**
     * set value for deleted 
     *
     * type:BIT,size:0,default:0
     *
     * @param mixed $deleted
     */
    public function setDeleted($deleted) {
        $this->deleted = $deleted;
    }

    /**
     * get value for deleted 
     *
     * type:BIT,size:0,default:0
     *
     * @return mixed
     */
    public function getDeleted() {
        return $this->deleted;
    }

    /**
     * set value for created_on 
     *
     * type:TIMESTAMP,size:19,default:0000-00-00 00:00:00
     *
     * @param mixed $createdOn
     */
    public function setCreatedOn($createdOn) {
        $this->createdOn = $createdOn;
    }

    /**
     * get value for created_on 
     *
     * type:TIMESTAMP,size:19,default:0000-00-00 00:00:00
     *
     * @return mixed
     */
    public function getCreatedOn() {
        return $this->createdOn;
    }

    /**
     * set value for updated_on 
     *
     * type:TIMESTAMP,size:19,default:CURRENT_TIMESTAMP
     *
     * @param mixed $updatedOn
     */
    public function setUpdatedOn($updatedOn) {
        $this->updatedOn = $updatedOn;
    }

    /**
     * get value for updated_on 
     *
     * type:TIMESTAMP,size:19,default:CURRENT_TIMESTAMP
     *
     * @return mixed
     */
    public function getUpdatedOn() {
        return $this->updatedOn;
    }

    /**
     * Get table name
     *
     * @return string
     */
    public static function getTableName() {
        return self::SQL_TABLE_NAME;
    }

    /**
     * Get array with field id as index and field name as value
     *
     * @return array
     */
    public static function getFieldNames() {
        return self::$FIELD_NAMES;
    }

    /**
     * Get array with field id as index and property name as value
     *
     * @return array
     */
    public static function getPropertyNames() {
        return self::$PROPERTY_NAMES;
    }

    /**
     * get the field name for the passed field id.
     *
     * @param int $fieldId
     * @param bool $fullyQualifiedName true if field name should be qualified by table name
     * @return string field name for the passed field id, null if the field doesn't exist
     */
    public static function getFieldNameByFieldId($fieldId, $fullyQualifiedName = true) {
        if (!array_key_exists($fieldId, self::$FIELD_NAMES)) {
            return null;
        }
        $fieldName = self::SQL_IDENTIFIER_QUOTE . self::$FIELD_NAMES[$fieldId] . self::SQL_IDENTIFIER_QUOTE;
        if ($fullyQualifiedName) {
            return self::SQL_IDENTIFIER_QUOTE . self::SQL_TABLE_NAME . self::SQL_IDENTIFIER_QUOTE . '.' . $fieldName;
        }
        return $fieldName;
    }

    /**
     * Get array with field ids of identifiers
     *
     * @return array
     */
    public static function getIdentifierFields() {
        return self::$PRIMARY_KEYS;
    }

    /**
     * Get array with field ids of autoincrement fields
     *
     * @return array
     */
    public static function getAutoincrementFields() {
        return self::$AUTOINCREMENT_FIELDS;
    }

    /**
     * Get array with field id as index and property type as value
     *
     * @return array
     */
    public static function getPropertyTypes() {
        return self::$PROPERTY_TYPES;
    }

    /**
     * Get array with field id as index and field type as value
     *
     * @return array
     */
    public static function getFieldTypes() {
        return self::$FIELD_TYPES;
    }

    /**
     * Assign default values according to table
     * 
     */
    public function assignDefaultValues() {
        $this->assignByArray(self::$DEFAULT_VALUES);
    }

    /**
     * return hash with the field name as index and the field value as value.
     *
     * @return array
     */
    public function toHash() {
        $array = $this->toArray();
        $hash = array();
        foreach ($array as $fieldId => $value) {
            $hash[self::$FIELD_NAMES[$fieldId]] = $value;
        }
        return $hash;
    }

    /**
     * return array with the field id as index and the field value as value.
     *
     * @return array
     */
    public function toArray() {
        return array(
            self::$PROPERTY_NAMES[self::FIELD_ID] => $this->getId(),
            self::$PROPERTY_NAMES[self::FIELD_PROFILE_ID] => $this->getProfileId(),
            self::$PROPERTY_NAMES[self::FIELD_LICENSE_ID] => $this->getLicenseId(),
            self::$PROPERTY_NAMES[self::FIELD_COMMENTS] => $this->getComments(),
            self::$PROPERTY_NAMES[self::FIELD_REFERENCE_NUMBER] => $this->getReferenceNumber(),
            self::$PROPERTY_NAMES[self::FIELD_EXPIRY_DATE] => $this->getExpiryDate(),
            self::$PROPERTY_NAMES[self::FIELD_DELETED] => $this->getDeleted(),
            self::$PROPERTY_NAMES[self::FIELD_CREATED_ON] => $this->getCreatedOn(),
            self::$PROPERTY_NAMES[self::FIELD_UPDATED_ON] => $this->getUpdatedOn());
    }

    /**
     * return array with the field id as index and the field value as value for the identifier fields.
     *
     * @return array
     */
    public function getPrimaryKeyValues() {
        return array(
            self::FIELD_ID => $this->getId());
    }

    /**
     * Assign values from array with the field id as index and the value as value
     *
     * @param array $array
     */
    public function assignByArray($array) {
        $result = array();
        foreach ($array as $fieldId => $value) {
            $result[self::$FIELD_NAMES[$fieldId]] = $value;
        }
        $this->assignByHash($result);
    }

    /**
     * Assign values from hash where the indexes match the tables field names
     *
     * @param array $result
     */
    public function assignByHash($result) {
        $this->setId($result['id']);
        $this->setProfileId($result['profile_id']);
        $this->setLicenseId($result['license_id']);
        $this->setComments($result['comments']);
        $this->setReferenceNumber($result['reference_number']);
        $this->setExpiryDate($result['expiry_date']);
        $this->setDeleted($result['deleted']);
        $this->setCreatedOn($result['created_on']);
        $this->setUpdatedOn($result['updated_on']);
    }

    /**
     * Bind all values to statement
     *
     * @param PDOStatement $stmt
     */
    protected function bindValues(PDOStatement &$stmt) {
        $stmt->bindValue(1, $this->getId());
        $stmt->bindValue(2, $this->getProfileId());
        $stmt->bindValue(3, $this->getLicenseId());
        $stmt->bindValue(4, $this->getComments());
        $stmt->bindValue(5, $this->getReferenceNumber());
        $stmt->bindValue(6, $this->getExpiryDate());
        $stmt->bindValue(7, $this->getDeleted());
        $stmt->bindValue(8, $this->getCreatedOn());
        $stmt->bindValue(9, $this->getUpdatedOn());
    }

}
