<?php

/**
 * Main application class.
 */
final class Index {

    const DEFAULT_PAGE = 'index';
    const PAGE_DIR = '../page/';
    const LAYOUT_DIR = '../layout/';


    /**
     * System config.
     */
    public function init() {
        // error reporting - all errors for development (ensure you have display_errors = On in your php.ini file)
        error_reporting(E_ALL | E_STRICT);
        mb_internal_encoding('UTF-8');
        set_exception_handler(array($this, 'handleException'));
        spl_autoload_register(array($this, 'loadClass'));
        
        Auth::sec_session_start();
        
    }

    /**
     * Run the application!
     */
    public function run() {
        $this->runPage($this->getPage());
    }

    /**
     * Exception handler.
     */
    public function handleException(Exception $ex) {
        $extra = array('message' => $ex->getMessage());
        if ($ex instanceof NotFoundException) {
            header('HTTP/1.0 404 Not Found');
            $this->runPage('404', $extra);
        } else {
            // Profile log exception
            header('HTTP/1.1 500 Internal Server Error');
            $this->runPage('500', $extra);
        }
    }

    /**
     * Class loader.
     */
    public function loadClass($name) {
        $classes = array(
            'Config' => '../config/Config.php',
            'Error' => '../validation/Error.php',
            'NotFoundException' => '../exception/NotFoundException.php',
            
            //For Profile table
            'ProfileDao' => '../dao/ProfileDao.php',
            'ProfileMapper' => '../mapping/ProfileMapper.php',
            'Profile' => '../model/Profile.php',
            'ProfileSearchCriteria' => '../dao/ProfileSearchCriteria.php',
            //'ProfileValidator' => '../validation/ProfileValidator.php',
            
            //For ProfileAttachment table
            'ProfileAttachmentDao' => '../dao/ProfileAttachmentDao.php',
            'ProfileAttachmentMapper' => '../mapping/ProfileAttachmentMapper.php',
            'ProfileAttachment' => '../model/ProfileAttachment.php',
            
            //For License table
            'LicenseDao' => '../dao/LicenseDao.php',
            'LicenseMapper' => '../mapping/LicenseMapper.php',
            'License' => '../model/License.php',
            
            //For ProfileLicense table
            'ProfileLicenseDao' => '../dao/ProfileLicenseDao.php',
            'ProfileLicenseMapper' => '../mapping/ProfileLicenseMapper.php',
            'ProfileLicense' => '../model/ProfileLicense.php',
            
            //Utils
            'Utils' => '../util/Utils.php',
            'Auth' => '../util/Auth.php',
            'CrmConnection' => '../util/CrmConnection.php',
        );
        if (!array_key_exists($name, $classes)) {
            die('Class "' . $name . '" not found.');
        }
        require_once $classes[$name];
    }

    private function getPage() {
        $page = self::DEFAULT_PAGE;
        if (Utils::hasUrlParam('page')) {
            $page = Utils::getUrlParam('page');
        }
        
        return $this->checkPage($page);
    }

    private function checkPage($page) {
        if (!preg_match('/^[a-z0-9-]+$/i', $page)) {
            // Profile log attempt, redirect attacker, ...
            throw new NotFoundException('Unsafe page "' . $page . '" requested');
        }
        if (!$this->hasScript($page) && !$this->hasTemplate($page)) {
            // Profile log attempt, redirect attacker, ...
            throw new NotFoundException('Page "' . $page . '" not found');
        }
        return $page;
    }

    private function runPage($page, array $extra = array()) {
        $run = false;
        if ($this->hasScript($page)) {
            $run = true;
            require $this->getScript($page);
        }
        if ($this->hasTemplate($page)) {
            $run = true;
            // data for main template
            $template = $this->getTemplate($page);
            require self::LAYOUT_DIR . 'index.phtml';
        }
        if (!$run) {
            die('Page "' . $page . '" has neither script nor template!');
        }
    }

    private function getScript($page) {
        return self::PAGE_DIR . $page . '.php';
    }

    private function getTemplate($page) {
        return self::PAGE_DIR . $page . '.phtml';
    }

    private function hasScript($page) {
        return file_exists($this->getScript($page));
    }

    private function hasTemplate($page) {
        return file_exists($this->getTemplate($page));
    }

}

$index = new Index();
$index->init();
// run application!
$index->run();
