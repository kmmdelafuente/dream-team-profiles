define(["text!./change_password.html"],

    function(Template) {

        /**
         * MODEL
         * =====
         * This is the main entry point, responsible for locating and processing
         * data related to the current portfolio.
         *
         * @constructor
         */
        function Model() {
            var self = this;
            
            // variables for form validation
            self.invalidArr = ko.observableArray();
            $("#change-password-container").on ('invalid.bs.validator', function(event) {
                self.invalidArr.push(event.relatedTarget.id);
            });
            $("#change-password-container").on ('valid.bs.validator', function(event) {
                self.invalidArr.remove(event.relatedTarget.id);
            });
            
            self.emailAddress = ko.observable("");
            self.password = ko.observable("");
            
            self.currentView = ko.observable("");
            self.recaptchaVerified = ko.observable(false);
            self.initialiseRecptcha = true;
            self.currentPage = ko.observable("");
            self.recaptchaScreenName = "change-password";
            
            /**
             * This destroys the validator of the form of the previous view.
             */
            self.currentView.subscribe(function(oldValue) {
                if(oldValue) {
                    $("#" + oldValue + "-form").validator("destroy");
                    $("#" + oldValue + "-form").off('invalid.bs.validator');
                    $("#" + oldValue + "-form").off('valid.bs.validator');
                }
            }, self, "beforeChange");
            
            /**
             * This creates the validator of the form of the current view.
             */
            self.currentView.subscribe(function(newValue) {
                self.invalidArr.removeAll();
                $("#" + newValue + "-form").validator();
                
                if(newValue == "check-email" && self.currentPage() == "change-password") {
                    resetRecaptcha(self.recaptchaScreenName);
                }
            });
            
            /**
             * This function returns the template to be used based on the
             * current view. The default view is "check-email".
             */
            self.currentTemplate = ko.computed(function() {
                if(!self.currentView()) {
                    self.currentView("check-email");
                }
                return self.currentView() + "-tpl";
            });
            
            /**
             * All fields will be set to blank.
             */
            self.resetScreen = function() {
                self.emailAddress("");
                self.password("");
                self.currentView("");
                self.recaptchaVerified(false);
            };
            
            /**
             * This function handles the click event of the Update Password
             * button. It will first validate the form, then send
             * a request to the server to save the new password.
             */
            self.updatePassword = function() {
                $('#' + self.currentView() + '-form').validator('validate');
                if(self.invalidArr().length > 0) {
                    ko.postbox.publish('ALERT_MSG', {type: "error", msg: "Please check the invalid fields."});
                    return false;
                }
                
                $.ajax({
                    url: "index.php", 
                    method: "post",
                    data: { page: "change-password", data: ko.toJSON(self) }
                }).done(function(data) {
                    var result = JSON.parse(data);
                    if(result.succeed) {
                        ko.postbox.publish('CURRENT_PAGE', "login");
                        ko.postbox.publish('ALERT_MSG', {type: "success", msg: "Your password has been successfully changed."});
                        self.resetScreen();
                    } else {
                        ko.postbox.publish('ALERT_MSG', { type: "error", msg: result.msg });
                    }
                });
            };
            
            /**
             * This function handles the click event of the Reset Password
             * button. It will send a request to the server to check if
             * the email address entered is registered in the system.
             */
            self.verifyEmail = function() {
                $('#' + self.currentView() + '-form').validator('validate');
                if(self.invalidArr().length > 0) {
                    ko.postbox.publish('ALERT_MSG', {type: "error", msg: "Please check the invalid fields."});
                    return false;
                }
                
                if(!self.recaptchaVerified()) {
                    ko.postbox.publish('ALERT_MSG', 
                            {type: "error", msg: "Please confirm that you are not a robot."});
                    return false;
                }
                
                $.ajax({
                    url: "index.php", 
                    method: "post",
                    data: { page: "verify-email", email: self.emailAddress() }
                }).done(function(data) {
                    if(data == "NULL") {
                        ko.postbox.publish('ALERT_MSG', { type: "error", msg: "Email not found." });
                        self.emailAddress("");
                    } else {
                        if(isValidEmailAddress(data)) {
                            self.currentView("change-password");
                            ko.postbox.publish('ALERT_MSG', { type: null });
                        }
                    }
                });
            };
            
            /**
             * This function handles the click event of the Back button.
             */
            self.goBack = function() {
                self.currentView("");
            };
            
            /**
             * This function redirects the screen to the login screen.
             */
            self.doCancel = function() {
                ko.postbox.publish('CURRENT_PAGE', "login");
                self.resetScreen();
            };
            
            ko.postbox.subscribe("CURRENT_PAGE", function(data) {
                self.currentPage(data);
                if(data == "change-password") {
                    if(self.initialiseRecptcha) {
                        initRecaptcha(self.recaptchaScreenName);
                        self.initialiseRecptcha = false;
                    } else {
                        resetRecaptcha(self.recaptchaScreenName);
                    }
                }
            });
            
            ko.postbox.subscribe("CHANGE_PASSWORD_RECAPTCHA_VERIFIED", function(data) {
                self.recaptchaVerified(data == "YES");
            });
        }
        return { viewModel: Model, template: Template };
    }
);
