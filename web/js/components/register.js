define(["text!./register.html"],

    function(Template) {

        /**
         * MODEL
         * =====
         * This is the main entry point, responsible for processing
         * data related to the registration.
         *
         * @constructor
         */
        function Model() {
            var self = this;
            self.emailAddress = ko.observable("");
            self.confirmEmail = ko.observable("");
            self.password = ko.observable("");
            self.recaptchaVerified = ko.observable(false);
            self.firstName = "";
            self.lastName = "";
            
            self.clearFields = function() {
                self.emailAddress("");
                self.confirmEmail("");
                self.password("");
                self.recaptchaVerified(false);
                resetRecaptcha('register');
            };
            
            // variables for form validation
            self.invalidArr = ko.observableArray();
            $('#reg-form').on ('invalid.bs.validator', function(event) {
                self.invalidArr.push(event.relatedTarget.id);
            });
            $('#reg-form').on ('valid.bs.validator', function(event) {
                self.invalidArr.remove(event.relatedTarget.id);
            });
            $('#reg-form').validator();
            
            ko.postbox.subscribe("REGISTER_RECAPTCHA_VERIFIED", function(data) {
                self.recaptchaVerified(data == "YES");
            });
            
            ko.postbox.subscribe("CURRENT_PAGE", function(data) {
                if(data == "login") {
                    self.clearFields();
                    $('#reg-form').validator("destroy");
                    $('#reg-form').validator();
                }
            });
            
            self.createProfile = function() {
                $('#reg-form').validator('validate');
                if(self.invalidArr().length > 0) {
                    ko.postbox.publish('ALERT_MSG', {type: "error", msg: "Please check the invalid fields."});
                    return false;
                }
                
                if(!self.recaptchaVerified()) {
                    ko.postbox.publish('ALERT_MSG', 
                            {type: "error", msg: "Please confirm that you are not a robot."});
                    return false;
                }
                
                $.ajax({
                    url: "index.php", 
                    method: "post",
                    data: { checkEmail: true, page: "save", data: ko.toJSON(self) }
                }).done(function(data) {
                    var result = JSON.parse(data);
                    if(result.succeed) {
                        ko.postbox.publish('CURRENT_USER', result.profile.emailAddress);
                        ko.postbox.publish('CURRENT_PAGE', "profile");
                    } else {
                        ko.postbox.publish('ALERT_MSG', { type: "error", msg: result.msg });
                    }
                });
                
            };
            
            initRecaptcha('register');
            
        }
        return { viewModel: Model, template: Template };
    }
);
