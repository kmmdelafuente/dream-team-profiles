define(["text!./main_app.html"],
    function (Template) {

        /**
         * MODEL
         * =====
         * This is the main entry point, responsible for the display of 
         * the different screens.
         *
         * @constructor
         */
        function Model() {
            var self = this;
            self.currentPage = ko.observable("");
            self.currentUser = ko.observable("");
            self.alertMsg = ko.observable("");
            self.alertType = ko.observable("");
            
            /**
             * This part checks if there is an existing session. If there is,
             * proceed to show the profile screen. Otherwise, show
             * the login screen.
             */
            $.ajax({
                url: "index.php",
                method: "post",
                data: {page: "main"}
            }).done(function (data) {
                if (data.toString() == "NULL") {
                    self.currentPage("login");
                } else {
                    self.currentPage("profile");
                    self.currentUser(data);
                }
            });

            ko.postbox.subscribe("CURRENT_PAGE", function (data) {
                self.currentPage(data);
                self.hideAlert();
            });
            
            /* This is the current logged in user */
            ko.postbox.subscribe("CURRENT_USER", function (data) {
                self.currentUser(data);
            });

            self.showAlert = function () {
                $("#message-container").fadeIn();
            };

            self.hideAlert = function () {
                $("#message-container").fadeOut();
                self.alertMsg("");
            };

            ko.postbox.subscribe("ALERT_MSG", function (data) {
                if(data.type) {
                    self.alertType(data.type);
                    self.alertMsg(data.msg);
                    self.showAlert();
                    scrollToTop();
                } else {
                    self.hideAlert();
                }
            });

            self.alertIcon = ko.pureComputed(function () {
                switch (self.alertType()) {
                    case "error":
                        return "glyphicon-exclamation-sign";
                    case "success":
                        return "glyphicon-check";
                    default:
                        return "glyphicon-info-sign";
                }
            }, this);

            self.alertCss = ko.pureComputed(function () {
                switch (self.alertType()) {
                    case "error":
                        return "alert-danger";
                    case "success":
                        return "alert-success";
                    default:
                        return "alert-info";
                }
            }, this);
        }
        return {viewModel: Model, template: Template};
    }
);
