define(["text!./reset_password.html"],

    function(Template) {

        function Model(params) {
            var self = this ;
            
            self.email = ko.observable("");
            
            self.invalidArr = ko.observableArray();
            $('#reset-password-form').on ('invalid.bs.validator', function(event) {
                self.invalidArr.push(event.relatedTarget.id);
            });
            $('#reset-password-form').on ('valid.bs.validator', function(event) {
                self.invalidArr.remove(event.relatedTarget.id);
            });
            $('#reset-password-form').validator();
            
            self.resetPassword = function() {
                $('#reset-password-form').validator('validate');
                if(self.invalidArr().length > 0) {
                    ko.postbox.publish('ALERT_MSG', {type: "error", msg: "Please check the invalid fields."});
                    return false;
                }
                
                $.ajax({
                    url: "index.php", 
                    method: "post",
                    data: { page: "verify-email", email: self.email() }
                }).done(function(data) {
                    if(data == "NULL") {
                        ko.postbox.publish('ALERT_MSG', { type: "error", msg: "Email not found." });
                        self.email("");
                    } else {
                        if(isValidEmailAddress(data)) {
                            ko.postbox.publish('CURRENT_PAGE', "change-password");
                            ko.postbox.publish('RESET_PASSWORD', data);
                        }
                    }
                });
            };
            
            self.goBack = function() {
                            ko.postbox.publish('CURRENT_PAGE', "login");
            };
        }
        return { viewModel: Model, template: Template };
    }
);
