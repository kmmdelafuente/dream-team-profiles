define(["text!./login.html"],

    function(Template) {

        /**
         * MODEL
         * =====
         * This is the main entry point, responsible for locating and processing
         * data related to the current portfolio.
         *
         * @constructor
         */
        function Model() {
            var self = this ;
            self.email = ko.observable("");
            self.password = ko.observable("");
            
            // variables for form validation
            self.invalidArr = ko.observableArray();
            $('#login-form').on ('invalid.bs.validator', function(event) {
                self.invalidArr.push(event.relatedTarget.id);
            });
            $('#login-form').on ('valid.bs.validator', function(event) {
                self.invalidArr.remove(event.relatedTarget.id);
            });
            $('#login-form').validator();

            self.doLogin = function(data, event) {
                $('#login-form').validator('validate');
                if(self.invalidArr().length > 0) {
                    ko.postbox.publish('ALERT_MSG', {type: "error", msg: "Please check the invalid fields."});
                    return false;
                }
                
                var loginData = ko.toJSON(self);
                $.ajax({
                    url: "index.php", 
                    method: "post",
                    data: { page: "login", credentials: loginData }
                }).done(function(data) {
                    var res = JSON.parse(data);
                    if(res.success) {
                        ko.postbox.publish('CURRENT_PAGE', "profile");
                        ko.postbox.publish('CURRENT_USER', res.user);
                    } else {
                        ko.postbox.publish('ALERT_MSG', {type: "error", msg: res.msg});
                    }
                    
                });
            };
            
            self.forgotPassword = function() {
                ko.postbox.publish('CURRENT_PAGE', "change-password");
            };
            
            ko.postbox.subscribe("CURRENT_PAGE", function(data) {
                if(data == "login") {
                    self.email("");
                    self.password("");
                    $('#login-form').validator("destroy");
                    $('#login-form').validator();
                }
            });
        }
        return { viewModel: Model, template: Template };
    }
);
