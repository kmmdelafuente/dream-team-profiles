define(["text!./profile.html", "alertify"],
        function (Template, alertify) {
            function License(licenseDetails) {
                var license = this;
                license.id = licenseDetails.id;
                license.type = licenseDetails.type;
                license.territory = licenseDetails.territory;
            }

            function Country(code, name, states) {
                var country = this;
                country.code = code;
                country.name = name;
                country.states = states;

                country.licenseStates = ko.computed(function () {
                    var states = [];
                    states.push(new State(country.code, 'National'));
                    if (country.states) {
                        states = states.concat(country.states());
                    }

                    return states;
                });
            }

            function State(code, name) {
                var state = this;
                state.code = code;
                state.name = name;
            }

            function Lead(code, name) {
                var lead = this;
                lead.code = code;
                lead.name = name;
            }

            function Profession(code, name) {
                var profession = this;
                profession.code = code;
                profession.name = name;
            }

            function Specialty(code, name) {
                var specialty = this;
                specialty.code = code;
                specialty.name = name;
            }

            function Language(code, name) {
                var language = this;
                language.code = code;
                language.name = name;
            }

            function ProfileLicense(parent, licenseDetails) {
                var license = this;
                license.id = ko.observable("");
                license.licenseId = null;
                license.profileId = ko.observable("");
                license.type = null;
                license.comments = ko.observable("");
                license.referenceNumber = ko.observable("");
                license.expiryDate = ko.observable("");
                license.deleted = ko.observable(false);
                license.licenseStatus = parent.licenseStatus();
                license.getExpiryDateId = function (index) {
                    return (license.licenseStatus == "active" ? "expires" : "expired") + "-on-" + index;
                };

                if (licenseDetails.id) {
                    license.id(licenseDetails.id);
                }
                if (licenseDetails.licenseId) {
                    license.licenseId = licenseDetails.licenseId;
                    license.type = parent.getLicenseType(license.licenseId);
                }
                if (licenseDetails.profileId) {
                    license.profileId(licenseDetails.profileId);
                }
                if (licenseDetails.comments) {
                    license.comments(licenseDetails.comments);
                }
                if (licenseDetails.referenceNumber) {
                    license.referenceNumber(licenseDetails.referenceNumber);
                }
                if (licenseDetails.expiryDate) {
                    var d = new Date(licenseDetails.expiryDate);
                    var expiryDateText = (d.getMonth() + 1) + "/" + d.getDate() + "/" + d.getFullYear();
                    license.expiryDate(expiryDateText);
                }
                if (licenseDetails.deleted) {
                    license.deleted(!(licenseDetails.deleted == "0"));
                }
                if (licenseDetails.licenseStatus) {
                    license.licenseStatus = licenseDetails.licenseStatus;
                }
            }

            function Attachment(attachmentDetails) {
                var attachment = this;
                attachment.id = ko.observable("");
                attachment.profileId = ko.observable("");
                attachment.type = ko.observable("");
                attachment.filename = ko.observable("");
                attachment.comments = ko.observable("");
                attachment.uploadedOn = ko.observable("");
                attachment.deleted = ko.observable(false);
                attachment.contents = ko.observable();
                attachment.filetype = ko.observable();
                
                attachment.originalFilename = "";
                attachment.originalContents = "";
                attachment.originalFiletype = "";

                if (attachmentDetails.id) {
                    attachment.id(attachmentDetails.id);
                }
                if (attachmentDetails.profileId) {
                    attachment.profileId(attachmentDetails.profileId);
                }
                if (attachmentDetails.type) {
                    attachment.type(attachmentDetails.type);
                }
                if (attachmentDetails.filename) {
                    attachment.filename(attachmentDetails.filename);
                    attachment.originalFilename = attachmentDetails.filename;
                }
                if (attachmentDetails.filetype) {
                    attachment.filetype(attachmentDetails.filetype);
                    attachment.originalFiletype = attachmentDetails.filetype;
                }
                if (attachmentDetails.contents) {
                    attachment.contents(attachmentDetails.contents);
                    attachment.originalContents = attachmentDetails.contents;
                }
                if (attachmentDetails.comments) {
                    attachment.comments(attachmentDetails.comments);
                }
                if (attachmentDetails.uploadedOn) {
                    var d = new Date(attachmentDetails.uploadedOn);
                    var uploadedOnText = (d.getMonth() + 1) + "/" + d.getDate() + "/" + d.getFullYear();
                    attachment.uploadedOn(uploadedOnText);
                }
                if (attachmentDetails.deleted) {
                    attachment.deleted(!(attachmentDetails.deleted == "0"));
                }

                // change a default option
                ko.fileBindings.defaultOptions.buttonText = 'Browse';

                attachment.fileData = ko.observable({
                    file: ko.observable(), // will be filled with a File object
                    // Read the files (all are optional, e.g: if you're certain that it is a text file, use only text:
                    binaryString: ko.observable(), // FileReader.readAsBinaryString(Blob|File) - The result property will contain the file/blob's data as a binary string. Every byte is represented by an integer in the range [0..255].
                    text: ko.observable(), // FileReader.readAsText(Blob|File, opt_encoding) - The result property will contain the file/blob's data as a text string. By default the string is decoded as 'UTF-8'. Use the optional encoding parameter can specify a different format.
                    dataURL: ko.observable(), // FileReader.readAsDataURL(Blob|File) - The result property will contain the file/blob's data encoded as a data URL.
                    arrayBuffer: ko.observable(), // FileReader.readAsArrayBuffer(Blob|File) - The result property will contain the file/blob's data as an ArrayBuffer object.

                    // a special observable (optional)
                    base64String: ko.observable() // just the base64 string, without mime type or anything else
                });

                attachment.fileData().file.subscribe(function (newval) {
                    if(newval) {
                        attachment.filename(newval.name);
                        attachment.filetype(newval.type);
                    } else {
                        attachment.filename(attachment.originalFilename);
                        attachment.filetype(attachment.originalFiletype);
                    }
                });
                
                attachment.fileData().base64String.subscribe(function(newval) {
                    if(newval) {
                        attachment.contents(newval);
                        console.log(newval);
                    } else {
                        attachment.contents(attachment.originalContents);
                        console.log("old");
                    }
                });
                
                attachment.getAttachment = function() {
                    location.href = "index.php?page=get-attachment&id=" + attachment.id();
                    return false;
                };
            }

            function Profile(params) {
                var profile = this;
                var skipValues = ["attachments", "licenses", "state"];

                //initialise select values
                profile.countryOptions = ko.observableArray();
                profile.usStateOptions = ko.observableArray();
                profile.caStateOptions = ko.observableArray();
                profile.professionOptions = ko.observableArray();
                profile.leadOptions = ko.observableArray();
                profile.languageOptions = ko.observableArray();
                profile.specialtyOptions = ko.observableArray();
                profile.licenseOptions = ko.observableArray();
                $.ajax({
                    url: "lov.php",
                    dataType: 'json',
                    async: false,
                    data: {value: "ALL"},
                    success: function (data) {
                        if (data.licenses) {
                            profile.licenseOptions.removeAll();
                            for (var key in data.licenses) {
                                profile.licenseOptions.push(new License(data.licenses[key]));
                            }
                        }
                        if (data.states) {
                            if (data.states.US) {
                                profile.usStateOptions.removeAll();
                                for (var key in data.states.US) {
                                    profile.usStateOptions.push(new State(key, data.states.US[key]));
                                }
                            }
                            if (data.states.CA) {
                                profile.caStateOptions.removeAll();
                                for (var key in data.states.CA) {
                                    profile.caStateOptions.push(new State(key, data.states.CA[key]));
                                }
                            }
                        }
                        if (data.country) {
                            profile.countryOptions.removeAll();
                            for (var key in data.country) {
                                var states = null;
                                if (key == "US") {
                                    states = profile.usStateOptions;
                                } else if (key == "CA") {
                                    states = profile.caStateOptions;
                                }
                                profile.countryOptions.push(new Country(key, data.country[key], states));
                            }
                        }
                        if (data.lead) {
                            profile.leadOptions.removeAll();
                            for (var key in data.lead) {
                                profile.leadOptions.push(new Lead(key, data.lead[key]));
                            }
                        }
                        if (data.professions) {
                            profile.professionOptions.removeAll();
                            for (var key in data.professions) {
                                profile.professionOptions.push(new Profession(key, data.professions[key]));
                            }
                        }
                        if (data.languages) {
                            profile.languageOptions.removeAll();
                            for (var key in data.languages) {
                                profile.languageOptions.push(new Language(key, data.languages[key]));
                            }
                        }
                        if (data.specialties) {
                            profile.specialtyOptions.removeAll();
                            for (var key in data.specialties) {
                                profile.specialtyOptions.push(new Specialty(key, data.specialties[key]));
                            }
                        }
                        $(".select2").select2();
                    }
                });

                profile.id = ko.observable("");
                profile.emailAddress = ko.observable("");
                profile.password = ko.observable("");
                profile.profession = ko.observable("");
                profile.firstName = ko.observable("");
                profile.middleName = ko.observable("");
                profile.lastName = ko.observable("");
                profile.homePhone = ko.observable("");
                profile.cellPhone = ko.observable("");
                profile.timeToCall = ko.observable("");
                profile.address = ko.observable("");
                profile.city = ko.observable("");
                profile.state = ko.observable("");
                profile.stateInput = ko.observable("");
                profile.caStateSelect = ko.observable("");
                profile.usStateSelect = ko.observable("");
                profile.zipCode = ko.observable("");
                profile.country = ko.observable("");
                profile.languages = ko.observable("");
                profile.languagesSelected = ko.observableArray();
                profile.languagesSelected.subscribe(function (newval) {
                    profile.languages(newval.join());
                    if (!profile.languages()) {
                        profile.languages("");
                    }
                });
                profile.otherLanguages = ko.observable("");
                profile.specialties = ko.observable("");
                profile.specialtiesSelected = ko.observableArray();
                profile.specialtiesSelected.subscribe(function (newval) {
                    profile.specialties(newval.join());
                    if (!profile.specialties()) {
                        profile.specialties("");
                    }
                });
                profile.startTime = ko.observable("");
                profile.totalHours = ko.observable("");
                profile.lead = ko.observable("");
                profile.deleted = ko.observable("");

                // Comments and Attachments section
                profile.comments = ko.observable("");
                profile.attachments = ko.observableArray();
                profile.initAttachmentJsFields = function () {
                    var elIndex = profile.attachments().length - 1;
                    initDatePicker($("#uploaded-on-" + elIndex));
                    $("#attachment-type-" + elIndex).select2({
                        placeholder: "Select a state",
                        allowClear: true
                    });
                };
                profile.addAttachment = function () {
                    profile.attachments.push(new Attachment({profileId: profile.id()}));
                    profile.initAttachmentJsFields();
                };
                profile.removeAttachment = function (attachment) {
                    if (objHasValues(attachment)) {
                        alertify.confirm("Are you sure you want to delete this record?", function (e) {
                            if (e) {
                                if (profile.id()) {
                                    attachment.deleted(true);
                                } else {
                                    profile.attachments.remove(attachment);
                                }
                            } else {
                                return false;
                            }
                        });
                    } else {
                        profile.attachments.remove(attachment);
                    }
                };
                profile.destroyAttachmentJsFields = function (dom, index, thisProfile) {
                    console.log(index, thisProfile.id());
                };

                // Show only attachments that are not deleted
                profile.attachmentRows = ko.computed(function () {
                    var attachments = [];
                    for (var ind = 0; ind < profile.attachments().length; ind++) {
                        if (!profile.attachments()[ind].deleted()) {
                            attachments.push(profile.attachments()[ind]);
                        }
                    }
                    return attachments;
                });

                // Active and Expired Licenses section
                profile.licenseTerritory = ko.observable("");
                profile.licenseId = ko.observable("");
                profile.isExistingLicense = ko.observable();

                //Everytime the modal loads, reset fields to their default values.
                $('#license-modal').on('shown.bs.modal', function () {
                    profile.isExistingLicense("true");
                    profile.licenseTerritory("");
                });
                profile.activeLicenses = ko.observableArray();
                profile.expiredLicenses = ko.observableArray();
                profile.licenses = [];
                profile.licenseStatus = ko.observable("");

                profile.activeLicenseRows = ko.computed(function () {
                    var licenses = [];
                    for (var ind = 0; ind < profile.activeLicenses().length; ind++) {
                        if (!profile.activeLicenses()[ind].deleted()) {
                            licenses.push(profile.activeLicenses()[ind]);
                        }
                    }
                    return licenses;
                });

                profile.expiredLicenseRows = ko.computed(function () {
                    var licenses = [];
                    for (var ind = 0; ind < profile.expiredLicenses().length; ind++) {
                        if (!profile.expiredLicenses()[ind].deleted()) {
                            licenses.push(profile.expiredLicenses()[ind]);
                        }
                    }
                    return licenses;
                });

                profile.initLicenseJsFields = function (licenseStatus) {
                    var licenseArr = null;
                    var expiryDatePrefix = null;
                    if (licenseStatus == "expired") {
                        licenseArr = profile.expiredLicenses;
                        expiryDatePrefix = "expired";
                    } else {
                        licenseArr = profile.activeLicenses;
                        expiryDatePrefix = "expires";
                    }
                    var elIndex = licenseArr().length - 1;
                    initDatePicker($("#" + expiryDatePrefix + "-on-" + elIndex));
                };


                profile.getLicenseType = function (licenseId) {
                    for (var key in profile.licenseOptions()) {
                        if (profile.licenseOptions()[key].id == licenseId) {
                            return profile.licenseOptions()[key].type;
                        }
                    }
                    return null;
                };

                /**
                 * Returns all the licenses whose territory is the same
                 * as the selected territory. This will serve as
                 * the list of values for the license/designation dropdown.
                 */
                profile.licenseOptionsByTerritory = ko.computed(function () {
                    var licenseArr = [];
                    for (var key in profile.licenseOptions()) {
                        if (profile.licenseOptions()[key].territory == profile.licenseTerritory()) {
                            licenseArr.push(profile.licenseOptions()[key]);
                        }
                    }
                    return licenseArr;
                });

                profile.setLicenseStatus = function (item, event) {
                    profile.licenseStatus($(event.target).data('value'));
                };

                profile.addLicenseBtnEnable = ko.computed(function () {
                    return profile.isExistingLicense() == "false" ||
                            (profile.isExistingLicense() == "true" &&
                                    profile.licenseTerritory() &&
                                    profile.licenseId());
                });

                profile.addLicense = function () {
                    var isActive = profile.licenseStatus() == "active";
                    var licenseArr = isActive ? profile.activeLicenses : profile.expiredLicenses;
                    var elId;
                    var licenseId = profile.licenseId();

                    if (profile.isExistingLicense() == "false") {
                        licenseId = "0";
                    }

                    if (isActive) {
                        licenseArr = profile.activeLicenses;
                        elId = "expires-on-";
                    } else {
                        licenseArr = profile.expiredLicenses;
                        elId = "expired-on-";
                    }

                    licenseArr.push(new ProfileLicense(profile, {licenseId: licenseId}));
                    initDatePicker($("#" + elId + (licenseArr().length - 1)));
                };

                profile.removeLicense = function (license, event) {
                    var licenseArr = (license.licenseStatus == "active") ? profile.activeLicenses : profile.expiredLicenses;
                    if (objHasValues(license)) {
                        alertify.confirm("Are you sure you want to delete this record?", function (e) {
                            if (e) {
                                if (license.id) {
                                    license.deleted(true);
                                } else {
                                    licenseArr.remove(license);
                                }
                            } else {
                                return false;
                            }
                        });
                    } else {
                        licenseArr.remove(license);
                    }
                };

                //Show state as a select field if the selected country is either CA or US,
                //and list their respective states. Otherwise, show state as an input field.
                profile.showStateField = ko.observable("input");
                profile.country.subscribe(function (newval) {
                    if (newval == "CA") {
                        profile.showStateField("caSelect");
                    } else if (newval == "US") {
                        profile.showStateField("usSelect");
                    } else {
                        profile.showStateField("input");
                    }
                });

                profile.showStateField = ko.observable("input");

                profile.doLogout = function () {
                    $.ajax({
                        url: "index.php",
                        method: "post",
                        data: {page: "logout"}
                    }).done(function (data) {
                        ko.postbox.publish("CURRENT_PAGE", "login");
                        profile.clearValues();
                    });
                };

                profile.clearValues = function () {
                    profile.id("");
                    profile.emailAddress("");
                    profile.password("");
                    profile.profession("");
                    profile.firstName("");
                    profile.middleName("");
                    profile.lastName("");
                    profile.homePhone("");
                    profile.cellPhone("");
                    profile.timeToCall("");
                    profile.address("");
                    profile.city("");
                    profile.state("");
                    profile.zipCode("");
                    profile.country("");
                    profile.languages("");
                    profile.languagesSelected.removeAll();
                    profile.otherLanguages("");
                    profile.specialties("");
                    profile.specialtiesSelected.removeAll();
                    profile.startTime("");
                    profile.totalHours("");
                    profile.lead("");
                    profile.deleted("");
                    profile.comments("");
                    profile.attachments.removeAll();
                    profile.activeLicenses.removeAll();
                    profile.expiredLicenses.removeAll();
                    profile.licenses = [];
                };

                profile.setProfileValues = function (data) {
                    objAssignValues(profile, data, skipValues);

                    switch (profile.showStateField()) {
                        case "input":
                            profile.stateInput(data.state);
                            break;
                        case "usSelect":
                            profile.usStateSelect(data.state);
                            break;
                        case "caSelect":
                            profile.caStateSelect(data.state);
                            break;
                    }

                    profile.attachments.removeAll();
                    for (var key in data.attachments) {
                        profile.attachments.push(new Attachment(data.attachments[key]));
                    }

                    profile.activeLicenses.removeAll();
                    profile.expiredLicenses.removeAll();
                    for (var key in data.licenses) {
                        var now = new Date();
                        var expiryDate = new Date(data.licenses[key].expiryDate);
                        if (expiryDate <= now) {
                            data.licenses[key].licenseStatus = "expired";
                            profile.expiredLicenses.push(new ProfileLicense(profile, data.licenses[key]));
                        } else {
                            data.licenses[key].licenseStatus = "active";
                            profile.activeLicenses.push(new ProfileLicense(profile, data.licenses[key]));
                        }
                    }
                };

                // variables for form validation
                profile.invalidArr = ko.observableArray();
                $('#profile-form').on('invalid.bs.validator', function (event) {
                    profile.invalidArr.push(event.relatedTarget.id);
                });
                $('#profile-form').on('valid.bs.validator', function (event) {
                    profile.invalidArr.remove(event.relatedTarget.id);
                });
                $('#profile-form').validator({
                    custom: {
                        checkdateformat: function (el) {
                            var id = el.context.id;
                            var inputDateValue = getDateValue($("#" + id).val(), "/");

                            if (!inputDateValue) {
                                return false;
                            }
                            return true;
                        },
                        checkexpirydate: function (el) {
                            var id = el.context.id;
                            var now = (new Date()).setHours(0, 0, 0, 0);
                            var res = id.split("-");
                            var inputDate = $("#" + id).val();
                            var inputDateValue = getDateValue(inputDate, "/");

                            if (!inputDateValue) {
                                return false;
                            }

                            if (res[0] == "expires" && inputDateValue <= now) {
                                return false;
                            } else if (res[0] == "expired" && inputDateValue > now) {
                                return false;
                            }

                            return true;
                        }
                    },
                    errors: {
                        checkdateformat: 'Please key in a valid date.',
                        checkexpirydate: 'Please key in a valid expiry date.'
                    }
                });

                profile.saveProfile = function () {
                    $('#profile-form').validator('validate');
                    if (profile.invalidArr().length > 0) {
                        ko.postbox.publish('ALERT_MSG', {type: "error", msg: "Please check the invalid fields."});
                        return false;
                    }

                    switch (profile.showStateField()) {
                        case "input":
                            profile.state(profile.stateInput());
                            break;
                        case "usSelect":
                            profile.state(profile.usStateSelect());
                            break;
                        case "caSelect":
                            profile.state(profile.caStateSelect());
                            break;
                    }

                    profile.licenses = profile.activeLicenses().concat(profile.expiredLicenses());

                    $.ajax({
                        url: "index.php",
                        method: "post",
                        data: {
                            page: "save",
                            data: ko.toJSON(profile,
                                    function (key, value) {
                                        if (key.search("Options") >= 0 || key.search("Rows") >= 0 ||
                                                key.search("Licenses") >= 0 || key == "invalidArr" ||
                                                key == "fileData") {
                                            return;
                                        } else {
                                            if (value == null) {
                                                return "";
                                            }
                                            return value;
                                        }
                                    })
                        }
                    }).done(function (data) {
                        var result = JSON.parse(data);
                        if (result.succeed) {
                            profile.setProfileValues(result.profile);
                            ko.postbox.publish('ALERT_MSG', {type: "success", msg: "Your profile has been successfully saved."});

                            // scroll to top to show message
                            $("html, body").animate({scrollTop: 0}, "slow");
                        } else {
                            alertify.alert(result.msg.toString());
                        }
                    });
                };

                ko.postbox.subscribe("CURRENT_USER", function (data) {
                    profile.emailAddress(data);
                });

                profile.mapProfileValues = function (data) {
                    profile.profession(data.profession);
                };

                profile.emailAddress.subscribe(function (newval) {
                    if (newval) {
                        $.ajax({
                            url: "index.php",
                            method: "post",
                            data: {page: "profile", type: "get"}
                        }).done(function (data) {
                            profile.setProfileValues(JSON.parse(data));

                            //set values for select multiple tags
                            var selectedArr = [];
                            if (profile.languages()) {
                                selectedArr = profile.languages().split(",");
                                profile.languagesSelected.removeAll();
                                for (var ind = 0; ind < selectedArr.length; ind++) {
                                    profile.languagesSelected.push(selectedArr[ind]);
                                }
                            }
                            if (profile.specialties()) {
                                selectedArr = profile.specialties().split(",");
                                profile.specialtiesSelected.removeAll();
                                for (var ind = 0; ind < selectedArr.length; ind++) {
                                    profile.specialtiesSelected.push(selectedArr[ind]);
                                }
                            }
                            $(".select2").trigger("change");
                        });
                    }
                });
                profile.emailAddress(params.currentUser());

            }

            return {viewModel: Profile, template: Template};
        }
);
