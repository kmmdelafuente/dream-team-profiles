define(["text!./login_main.html"],

    function(Template) {

        /**
         * MODEL
         * =====
         * This is the main entry point, responsible for locating and processing
         * data related to the current portfolio.
         *
         * @param params
         * @constructor
         */
        function Model(params) {
            var self = this;
        }
        return { viewModel: Model, template: Template };
    }
);
