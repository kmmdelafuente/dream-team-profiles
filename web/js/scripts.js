var dtpRecaptcha = [];

formatScreenName = function(screen) {
    return screen.replace("-", "_").toUpperCase();
};

initRecaptcha = function(screen) {
    var screenCaps = formatScreenName(screen);
    try {
        dtpRecaptcha[screen] = grecaptcha.render(screen + '-recaptcha', {
            'sitekey' : '6LcvCwwTAAAAAI2Ju5OXqLsBeMikTeUkHZlIrTC0',
            'callback' : function(response) {
                recaptchaCallback(response, screenCaps);
            },
            'expired-callback' : function(response) {
                recaptchaExpired(response, screenCaps);
            }
        });
    } catch(e) {
        
    }
};

resetRecaptcha = function(screen) {
    try {
        grecaptcha.reset(dtpRecaptcha[screen]);
        recaptchaExpired(formatScreenName(screen));
    } catch(e) {
        
    }
};

initDatePicker = function(el) {
    el.datepicker()
        .on('changeDate', function(e) {
            $(this).trigger('change');
            $(this).datepicker('hide');
         });
};

recaptchaCallback = function(response, screen) {
//    $.ajax({
//        url: "https://www.google.com/recaptcha/api/siteverify", 
//        method: "post",
//        data: { response: response, secret: "6LcvCwwTAAAAAHjAz8V1AmZOCL0da2ajLVEe7NS3" }
//    }).done(function(data) {
//        console.log(data);
//    });
    ko.postbox.publish(screen + "_RECAPTCHA_VERIFIED", "YES");
    
};

recaptchaExpired = function(screen) {
    ko.postbox.publish(screen + "_RECAPTCHA_VERIFIED", "NO");
};

isValidEmailAddress = function(emailAddress) {
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    return pattern.test(emailAddress);
};

scrollToTop = function() {
    $("html, body").animate({ scrollTop:0 }, "slow");
};

getDateValue = function (txtDate, separator) {
    var aoDate,           // needed for creating array and object
        ms,               // date in milliseconds
        month, day, year; // (integer) month, day and year
    // if separator is not defined then set '/'
    if (separator === undefined) {
        separator = '/';
    }
    // split input date to month, day and year
    aoDate = txtDate.split(separator);
    // array length should be exactly 3 (no more no less)
    if (aoDate.length !== 3) {
        return null;
    }
    // define month, day and year from array (expected format is m/d/yyyy)
    // subtraction will cast variables to integer implicitly
    month = aoDate[0] - 1; // because months in JS start from 0
    day = aoDate[1] - 0;
    year = aoDate[2] - 0;
    // test year range
    if (year < 1000 || year > 3000) {
        return null;
    }
    // convert input date to milliseconds
    ms = (new Date(year, month, day)).getTime();
    // initialize Date() object from milliseconds (reuse aoDate variable)
    aoDate = new Date();
    aoDate.setTime(ms);
    // compare input date and parts from Date() object
    // if difference exists then input date is not valid
    if (aoDate.getFullYear() !== year ||
        aoDate.getMonth() !== month ||
        aoDate.getDate() !== day) {
        return null;
    }
    // date is OK, return true
    return aoDate;
};