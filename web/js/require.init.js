/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

requirejs.config({
    //By default load any module IDs from js/lib
    baseUrl: 'js/',
    paths: {
        alertify: 'alertify',
        ko: 'knockout',
        jquery: 'jquery.min',
        bootstrap: 'bootstrap.min',
        postbox: 'knockout-postbox',
        hasher: 'hasher',
        datepicker: 'bootstrap-datepicker',
        "jquery-form": 'jquery.form.min',
        "file-bindings": 'knockout-file-bindings'
    }
});

define([], // These are here to force initialisation

        function () { // }, bs, kp, common, mydb, app) {
            var startup = this;

            ko.bindingHandlers.fadeVisible = {
                init: function (element, valueAccessor) {
                    // Initially set the element to be instantly visible/hidden depending on the value
                    var value = valueAccessor();
                    $(element).toggle(ko.unwrap(value)); // Use "unwrapObservable" so we can handle values that may or may not be observable
                },
                update: function (element, valueAccessor) {
                    // Whenever the value subsequently changes, slowly fade the element in or out
                    var value = valueAccessor();
                    ko.unwrap(value) ? $(element).fadeIn() : $(element).hide();
                }
            };

            ko.bindingHandlers.option = {
                update: function (element, valueAccessor) {
                    var value = ko.utils.unwrapObservable(valueAccessor());
                    ko.selectExtensions.writeValue(element, value);
                }
            };

            ko.components.register('main-app', {require: 'components/main_app'});
            ko.components.register('login-main', {require: 'components/login_main'});
            ko.components.register('login', {require: 'components/login'});
            ko.components.register('register', {require: 'components/register'});
            ko.components.register('change-password', {require: 'components/change_password'});
            ko.components.register('reset-password', {require: 'components/reset_password'});
            ko.components.register('profile', {require: 'components/profile'});

            ko.applyBindings({});
        }
);