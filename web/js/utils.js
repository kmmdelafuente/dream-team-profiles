/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function toTitleCase(str)
{
    return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
}

/**
 * objHasValues
 * ============
 * Returns true if the value of one of the object elements is not null.
 * Otherwise, return false.
 * 
 * @param Object obj
 * @returns {Boolean}
 */
function objHasValues(obj) {
    for(var key in obj) {
        var value = (ko.isObservable(obj[key]) ? obj[key]() : obj[key]);
        if(value) {
            return true;
        }
    }
    return false;
}

function objAssignValues(obj, values, skipValues) {
    for(var key in obj) {
        if($.inArray(key, skipValues) >= 0) {
            continue;
        }
        
        var assignValue = ko.isObservable(obj[key]) || !isFunction(obj[key]);
        if(assignValue) {
            var value = "";
            if(values[key]) {
                value = values[key];
            } else {
                continue;
            }
            if(ko.isObservable(obj[key])) {
                obj[key](value);
            } else {
                obj[key] = value;
            }
        }
    }
    return false;
}

function isFunction(functionToCheck) {
    var getType = {};
    return functionToCheck && getType.toString.call(functionToCheck) === '[object Function]';
}
