<?php
/**
 * Mapper for {@link License} from array.
 */
final class LicenseMapper {

    private function __construct() {
    }
    
    /**
     * Maps array to the given {@link License}.
     * <p>
     * Expected properties are:
     * <ul>
     *   <li>id</li>
     *   <li>territory</li>
     *   <li>type</li>
     *   <li>deleted</li>
     *   <li>created_on</li>
     *   <li>updated_on</li>
     * </ul>
     * @param License $license
     * @param array $properties
     */
    public static function map(License $license, array $properties) {
        if (array_key_exists('id', $properties)) {
            $license->setId($properties['id']);
        }
        if (array_key_exists('territory', $properties)) {
            $license->setTerritory($properties['territory']);
        }
        if (array_key_exists('type', $properties)) {
            $license->setType($properties['type']);
        }
        if (array_key_exists('deleted', $properties)) {
            $license->setDeleted($properties['deleted']);
        }
        if (array_key_exists('created_on', $properties)) {
            $createdOn = self::createDateTime($properties['created_on']);
            if ($createdOn) {
                $license->setCreatedOn($createdOn);
            }
        }
        if (array_key_exists('updated_on', $properties)) {
            $updatedOn = self::createDateTime($properties['updated_on']);
            if ($updatedOn) {
                $license->setUpdatedOn($updatedOn);
            }
        }
    }

    private static function createDateTime($input) {
        return DateTime::createFromFormat('Y-n-j H:i:s', $input);
    }

}
