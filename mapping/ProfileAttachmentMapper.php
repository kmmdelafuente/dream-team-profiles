<?php
/**
 * Mapper for {@link ProfileAttachment} from array.
 */
final class ProfileAttachmentMapper {

    private function __construct() {
    }
    
    /**
     * Maps array to the given {@link ProfileAttachment}.
     * <p>
     * Expected properties are:
     * <ul>
     *   <li>id</li>
     *   <li>profile_id</li>
     *   <li>type</li>
     *   <li>filename</li>
     *   <li>filetype</li>
     *   <li>contents</li>
     *   <li>comments</li>
     *   <li>uploaded_on</li>
     *   <li>deleted</li>
     *   <li>created_on</li>
     *   <li>updated_on</li>
     * </ul>
     * @param ProfileAttachment $profileAttachment
     * @param array $properties
     */
    public static function map(ProfileAttachment $profileAttachment, array $properties) {
        if (array_key_exists('id', $properties)) {
            $profileAttachment->setId($properties['id']);
        }
        if (array_key_exists('profile_id', $properties)) {
            $profileAttachment->setProfileId($properties['profile_id']);
        }
        if (array_key_exists('type', $properties)) {
            $profileAttachment->setType($properties['type']);
        }
        if (array_key_exists('filename', $properties)) {
            $profileAttachment->setFilename(addslashes($properties['filename']));
        }
        if (array_key_exists('filetype', $properties)) {
            $profileAttachment->setFiletype($properties['filetype']);
        }
        if (array_key_exists('contents', $properties)) {
            $profileAttachment->setContents(addslashes($properties['contents']));
        }
        if (array_key_exists('comments', $properties)) {
            $profileAttachment->setComments($properties['comments']);
        }
        if (array_key_exists('deleted', $properties)) {
            $profileAttachment->setDeleted($properties['deleted']);
        }
        if (array_key_exists('uploaded_on', $properties)) {
            $uploadedOn = date("Y-m-d", strtotime($properties['uploaded_on']));
            if($uploadedOn) {
                $profileAttachment->setUploadedOn($uploadedOn);
            }
        }
        if (array_key_exists('created_on', $properties)) {
            $createdOn = self::createDateTime($properties['created_on']);
            if ($createdOn) {
                $profileAttachment->setCreatedOn($createdOn);
            }
        }
        if (array_key_exists('updated_on', $properties)) {
            $updatedOn = self::createDateTime($properties['updated_on']);
            if ($updatedOn) {
                $profileAttachment->setUpdatedOn($updatedOn);
            }
        }
    }

    private static function createDateTime($input) {
        return DateTime::createFromFormat('Y-n-j H:i:s', $input);
    }

}
