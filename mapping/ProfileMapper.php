<?php

/**
 * Mapper for {@link Profile} from array.
 * @see ProfileValidator
 */
final class ProfileMapper {

    private function __construct() {
    }

    /**
     * Maps array to the given {@link Profile}.
     * <p>
     * Expected properties are:
     * <ul>
     *   <li>id</li>
     *   <li>password</li>
     *   <li>first_name</li>
     *   <li>middle_name</li>
     *   <li>last_name</li>
     *   <li>home_phone</li>
     *   <li>cell_phone</li>
     *   <li>time_to_call</li>
     *   <li>profession</li>
     *   <li>address</li>
     *   <li>city</li>
     *   <li>state</li>
     *   <li>country</li>
     *   <li>email_address</li>
     *   <li>zip_code</li>
     *   <li>languages</li>
     *   <li>other_languages</li>
     *   <li>specialties</li>
     *   <li>start_time</li>
     *   <li>total_hours</li>
     *   <li>lead</li>
     *   <li>comments</li>
     *   <li>deleted</li>
     *   <li>created_on</li>
     *   <li>updated_on</li>
     * </ul>
     * @param Profile $profile
     * @param array $properties
     */
    public static function map(Profile $profile, array $properties) {
        if (array_key_exists('id', $properties)) {
            $profile->setId($properties['id']);
        }
        if (array_key_exists('password', $properties)) {
            $profile->setPassword($properties['password']);
        }
        if (array_key_exists('first_name', $properties)) {
            $profile->setFirstName($properties['first_name']);
        }
        if (array_key_exists('middle_name', $properties)) {
            $profile->setMiddleName($properties['middle_name']);
        }
        if (array_key_exists('last_name', $properties)) {
            $profile->setLastName($properties['last_name']);
        }
        if (array_key_exists('home_phone', $properties)) {
            $profile->setHomePhone($properties['home_phone']);
        }
        if (array_key_exists('cell_phone', $properties)) {
            $profile->setCellPhone($properties['cell_phone']);
        }
        if (array_key_exists('time_to_call', $properties)) {
            $profile->setTimeToCall($properties['time_to_call']);
        }
        if (array_key_exists('profession', $properties)) {
            $profile->setProfession($properties['profession']);
        }
        if (array_key_exists('address', $properties)) {
            $profile->setAddress($properties['address']);
        }
        if (array_key_exists('city', $properties)) {
            $profile->setCity($properties['city']);
        }
        if (array_key_exists('state', $properties)) {
            $profile->setState($properties['state']);
        }
        if (array_key_exists('country', $properties)) {
            $profile->setCountry($properties['country']);
        }
        if (array_key_exists('email_address', $properties)) {
            $profile->setEmailAddress($properties['email_address']);
        }
        if (array_key_exists('zip_code', $properties)) {
            $profile->setZipCode($properties['zip_code']);
        }
        if (array_key_exists('languages', $properties)) {
            $profile->setLanguages($properties['languages']);
        }
        if (array_key_exists('other_languages', $properties)) {
            $profile->setOtherLanguages($properties['other_languages']);
        }
        if (array_key_exists('specialties', $properties)) {
            $profile->setSpecialties($properties['specialties']);
        }
        if (array_key_exists('start_time', $properties)) {
            $profile->setStartTime($properties['start_time']);
        }
        if (array_key_exists('total_hours', $properties)) {
            $profile->setTotalHours($properties['total_hours']);
        }
        if (array_key_exists('lead', $properties)) {
            $profile->setLead($properties['lead']);
        }
        if (array_key_exists('comments', $properties)) {
            $profile->setComments($properties['comments']);
        }
        if (array_key_exists('deleted', $properties)) {
            $profile->setDeleted($properties['deleted']);
        }
        if (array_key_exists('created_on', $properties)) {
            $createdOn = self::createDateTime($properties['created_on']);
            if ($createdOn) {
                $profile->setCreatedOn($createdOn);
            }
        }
        if (array_key_exists('updated_on', $properties)) {
            $updatedOn = self::createDateTime($properties['updated_on']);
            if ($updatedOn) {
                $profile->setUpdatedOn($updatedOn);
            }
        }
        if (array_key_exists('attachments', $properties)) {
            foreach ($properties['attachments'] as $attachment) {
                $attachmentData = array();
                foreach ($attachment as $key => $value) {
                    $attachmentData[Utils::from_camel_case($key)] = $value;
                }
                $profileAttachment = new ProfileAttachment();
                ProfileAttachmentMapper::map($profileAttachment, $attachmentData);
                $profile->addAttachment($profileAttachment);
            }
        }
        if (array_key_exists('licenses', $properties)) {
            foreach ($properties['licenses'] as $license) {
                $licenseData = array();
                foreach ($license as $key => $value) {
                    $licenseData[Utils::from_camel_case($key)] = $value;
                }
                $profileLicense = new ProfileLicense();
                ProfileLicenseMapper::map($profileLicense, $licenseData);
                $profile->addLicense($profileLicense);
            }
        }
    }

    private static function createDateTime($input) {
        return DateTime::createFromFormat('Y-n-j H:i:s', $input);
    }

}
