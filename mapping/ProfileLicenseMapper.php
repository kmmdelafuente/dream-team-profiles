<?php
/**
 * Mapper for {@link ProfileLicense} from array.
 * @see ProfileValidator
 */
final class ProfileLicenseMapper {

    private function __construct() {
    }
    
    /**
     * Maps array to the given {@link ProfileLicense}.
     * <p>
     * Expected properties are:
     * <ul>
     *   <li>id</li>
     *   <li>profile_id</li>
     *   <li>license_id</li>
     *   <li>comments</li>
     *   <li>reference_number</li>
     *   <li>expiry_date</li>
     *   <li>deleted</li>
     *   <li>created_on</li>
     *   <li>updated_on</li>
     * </ul>
     * @param ProfileLicense $profileLicense
     * @param array $properties
     */
    public static function map(ProfileLicense $profileLicense, array $properties) {
        if (array_key_exists('id', $properties)) {
            $profileLicense->setId($properties['id']);
        }
        if (array_key_exists('profile_id', $properties)) {
            $profileLicense->setProfileId($properties['profile_id']);
        }
        if (array_key_exists('license_id', $properties)) {
            $profileLicense->setLicenseId($properties['license_id']);
        }
        if (array_key_exists('comments', $properties)) {
            $profileLicense->setComments($properties['comments']);
        }
        if (array_key_exists('reference_number', $properties)) {
            $profileLicense->setReferenceNumber($properties['reference_number']);
        }
        if (array_key_exists('deleted', $properties)) {
            $profileLicense->setDeleted($properties['deleted']);
        }
        if (array_key_exists('expiry_date', $properties)) {
            $expiryDate = date("Y-m-d", strtotime($properties['expiry_date']));
            if($expiryDate) {
                $profileLicense->setExpiryDate($expiryDate);
            }
        }
        if (array_key_exists('created_on', $properties)) {
            $createdOn = self::createDateTime($properties['created_on']);
            if ($createdOn) {
                $profileLicense->setCreatedOn($createdOn);
            }
        }
        if (array_key_exists('updated_on', $properties)) {
            $updatedOn = self::createDateTime($properties['updated_on']);
            if ($updatedOn) {
                $profileLicense->setUpdatedOn($updatedOn);
            }
        }
    }

    private static function createDateTime($input) {
        return DateTime::createFromFormat('Y-n-j H:i:s', $input);
    }

}
