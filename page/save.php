<?php

$dao = new ProfileDao();
$data = json_decode(Utils::getUrlParam('data'), true);
$checkEmail = false;
if(Utils::hasUrlParam('checkEmail')) {
    $checkEmail = Utils::getUrlParam('checkEmail');
}

$profileData = array();
foreach ($data as $key => $value) {
    $profileData[Utils::from_camel_case($key)] = $value;
}

if($checkEmail) {
    $profile = $dao->findByEmail($profileData['email_address']);
    
    if($profile) {
        echo json_encode(array(
            "succeed" => false, 
            "msg" => "There is an existing profile for " . $profileData['email_address'] . "."));
        return;
    }
}

$profile = new Profile();
ProfileMapper::map($profile, $profileData);
$result = $dao->save($profile);

$loginData->email = $profile->getEmailAddress();
$loginData->password = $profile->getPassword();
Auth::validateLogin($loginData);

echo json_encode($result);