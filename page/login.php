<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if(Utils::hasUrlParam("credentials")) {
    $loginData = Utils::getUrlParam("credentials");
    $loginData = json_decode($loginData);     
    echo json_encode(Auth::validateLogin($loginData));
} else {
    echo "NULL";
}