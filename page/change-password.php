<?php

$data = json_decode(Utils::getUrlParam('data'), true);
$dao = new ProfileDao();
$profile = $dao->findByEmail($data['emailAddress']);
$profile->setPassword($data['password']);
$result = $dao->changePassword($profile);

echo json_encode($result);
