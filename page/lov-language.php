<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$valuesArr = array();
try {
    $value = Utils::getUrlParam('value');

    switch ($value) {
        case 'profession':
            $valuesArr = CrmConnection::getProfessions();
            break;
        case 'languages':
            $valuesArr = CrmConnection::getLanguages();
            break;
        case 'specialties':
            $valuesArr = CrmConnection::getSpecialties();
            break;
        case 'lead':
            $valuesArr = CrmConnection::getLeads();
            break;
        case 'country':
            $valuesArr = CrmConnection::getCountries();
            break;
        case 'state':
            try {
                $country = Utils::getUrlParam('country');
                $valuesArr = CrmConnection::getStates($country);
            } catch (Exception $exc) {
            }

            break;
        case 'license':
            $territory = null;
            if(Utils::hasUrlParam('territory')) {
                $territory = Utils::getUrlParam('territory');
            }
            $valuesArr = CrmConnection::getLicenses($territory);
            break;
        case 'ALL':
            $valuesArr['professions'] = CrmConnection::getProfessions();
            $valuesArr['languages'] = CrmConnection::getLanguages();
            $valuesArr['specialties'] = CrmConnection::getSpecialties();
            $valuesArr['lead'] = CrmConnection::getLeads();
            $valuesArr['country'] = CrmConnection::getCountries();
            $valuesArr['states'] = CrmConnection::getStates();
            $valuesArr['licenses'] = CrmConnection::getLicenses();
    }
} catch (Exception $e) {
}

echo json_encode($valuesArr);
