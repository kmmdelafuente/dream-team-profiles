<?php

$attachmentId = Utils::getUrlParam("id");
$result = "";
if($attachmentId) {
    $dao = new ProfileAttachmentDao();
    $attachment = $dao->findById($attachmentId);
    
    if(!$attachment) {
        $result = "NULL";
    } else {
        header("Content-type: " . $attachment->getFiletype());
        header("Content-Disposition: attachment; filename=\"" . $attachment->getFilename() . "\"");
        echo base64_decode($attachment->getContents());
 
        exit;
    }
} else {
    $result = "NULL";
}


echo $result;
