<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

final class Auth {
    
    const SESSION_NAME = "sec_session_id";

    private function __construct() {
        echo "contruct\n";
    }
    
    public static function sec_session_start() {
        // This stops JavaScript being able to access the session id.
        $httponly = true;
        // Forces sessions to only use cookies.
        if (ini_set('session.use_only_cookies', 1) === FALSE) {
            header("Location: ../error.php?err=Could not initiate a safe session (ini_set)");
            exit();
        }
        // Gets current cookies params.
        $cookieParams = session_get_cookie_params();
        session_set_cookie_params($cookieParams["lifetime"],
            $cookieParams["path"], 
            $cookieParams["domain"], 
            FALSE,
            $httponly);
        // Sets the session name to the one set above.
        session_name(self::SESSION_NAME);
        session_start();            // Start the PHP session 
        session_regenerate_id();    // regenerated the session, delete the old one. 
    }
    
    public static function validateLogin($loginData) {
        $dao = new ProfileDao();
        $profile = $dao->findByEmail($loginData->email);
        
        if($profile) {
            if($loginData->password == $profile->getPassword()) {
                self::setSessionUser($profile->getEmailAddress(), $profile->getPassword());
                return array("success" => true, "user" => $profile->getEmailAddress());
            } else {
                return array("success" => false, "msg" => "Invalid password.");
            }
        } else {
            return array("success" => false, "msg" => "Email address not found.");
        }
        
    }
    
    private static function setSessionUser($email, $password) {
        $_SESSION["user"] = $email;
        $user_browser = $_SERVER['HTTP_USER_AGENT'];
        $_SESSION['login_string'] = hash('sha512', $password . $user_browser);
    }
    
    public static function getSessionUser() {
        return $_SESSION["user"] ? $_SESSION["user"] : "NULL";
    }
    
    public static function logout() {
        // Unset all of the session variables.
        $_SESSION = array();

        // If it's desired to kill the session, also delete the session cookie.
        // Note: This will destroy the session, and not just the session data!
        if (ini_get("session.use_cookies")) {
            $params = session_get_cookie_params();
            setcookie(session_name(), '', time() - 42000,
                $params["path"], $params["domain"],
                $params["secure"], $params["httponly"]
            );
        }

        // Finally, destroy the session.
        session_destroy();
    }
    
    public static function hasActiveSession() {
        if (session_status() == PHP_SESSION_NONE) {
            return false;
        } else {
            if(self::getSessionUser()) {
                return true;
            } else {
                self::logout();
                return false;
            }
        }
    }
    
    public static function isLoggedIn() {
        // Check if all session variables are set 
        if (isset($_SESSION['user'], $_SESSION['login_string'])) {
            return true;
        } else {
            // Not logged in 
            return false;
        }
    }
    
}